@echo off
REM ---------------------------------------------------------------------------
REM  Script for building/running dbpad.
REM
REM Environment Variable Prequisites:
REM
REM   JAVA_HOME     Must point at your Java Development Kit installation.
REM
REM ---------------------------------------------------------------------------
REM $Id: dbpad.bat,v 1.3 2001/01/30 06:13:38 hyoon Exp $


REM ----- Save Environment Variables That May Change --------------------------
if "%DBPAD_HOME%" == "" set DBPAD_HOME=F:\dbpad\app
set _DBPAD_HOME=%DBPAD_HOME%
set _JAVAHELP_LIB=%_DBPAD_HOME%\lib\jhall.jar
set _JDBC_LIB=%_DBPAD_HOME%\lib\jdbc2_0-stdext.jar
set _CLASSPATH=%CLASSPATH%

REM ----- Verify and Set Required Environment Variables -----------------------
if "%JAVA_HOME%" == "" goto javahomeerror

REM ----- Set Up The Runtime Classpath ----------------------------------------
REM set CLASSPATH=%CLASSPATH%;%_DBPAD_HOME%\src
set CLASSPATH=%_DBPAD_HOME%\src
set CLASSPATH=%CLASSPATH%;%JAVA_HOME%\lib\tools.jar;%_JAVAHELP_LIB%
if exist %_JDBC_LIB% set CLASSPATH=%CLASSPATH%;%_JDBC_LIB%
REM echo Using CLASSPATH: %CLASSPATH%

REM ----- Prepare Appropriate Java Execution Commands -------------------------
set _JAVAC=classic
set _START_CMD=start %JAVA_HOME%\bin\java -cp "%CLASSPATH%"
set _RUN_CMD=%JAVA_HOME%\bin\java -cp "%CLASSPATH%"
set _BUILD_CMD=%JAVA_HOME%\bin\javac -classpath "%CLASSPATH%"
set _JAR_CMD=%JAVA_HOME%\bin\jar
set _DOC_CMD=%JAVA_HOME%\bin\javadoc -classpath "%CLASSPATH%" -private -version -author -windowtitle "DBPad in Java API Specification" -doctitle "DBPad in Java"
if "%OS%" == "Windows_NT" set _CLEAN_CMD=del /S /Q
if "%OS%" == "" set _CLEAN_CMD=del

REM ----- Execute The Requested Command ---------------------------------------
if "%1" == "env" goto doEnv
if "%1" == "build" goto doBuild
if "%1" == "jar" goto doJar
if "%1" == "javadoc" goto doJavadoc
if "%1" == "clean" goto doClean
if "%1" == "run" goto doRun
if "%1" == "start" goto doStart
if "%1" == "stop" goto doStop

:doUsage
echo Usage:  dbpad " env | build | jar | javadoc | clean | run | start | stop "
echo Commands:
echo   env     -  Set up environment variables that dbpad would use
echo   build   -  Build all dbpad classes and the jar file
echo   jar     -  Create dbpad.jar
echo   javadoc -  Generate api docs.
echo   clean   -  Clean all dbpad classes
echo   run     -  Start dbpad in the current window
echo   start   -  Start dbpad in a separate window
echo   stop    -  Stop dbpad
goto cleanup

:doEnv
goto finish

:doBuild
echo Building dbpad...
REM %_CLEAN_CMD% %_DBPAD_HOME%\src\com\bluecraft\dbpad\*.class
%_BUILD_CMD% %_DBPAD_HOME%\src\com\bluecraft\dbpad\file\*.java
%_BUILD_CMD% %_DBPAD_HOME%\src\com\bluecraft\dbpad\resource\*.java
%_BUILD_CMD% %_DBPAD_HOME%\src\com\bluecraft\dbpad\util\*.java
%_BUILD_CMD% %_DBPAD_HOME%\src\com\bluecraft\dbpad\*.java
REM echo Done.
REM goto cleanup

:doJar
echo Creating jar file...
%_CLEAN_CMD% %_DBPAD_HOME%\src\com\bluecraft\dbpad\*.*~
%_CLEAN_CMD% %_DBPAD_HOME%\build\dbpad.jar
cd %_DBPAD_HOME%\src
%_JAR_CMD% cfm %_DBPAD_HOME%\build\dbpad.jar %_DBPAD_HOME%\bin\dbpad.mf -C %_DBPAD_HOME%\src\ com\bluecraft\dbpad\*.class com\bluecraft\dbpad\file\*.class com\bluecraft\dbpad\resource\*.class com\bluecraft\dbpad\util\*.class com\bluecraft\dbpad\images com\bluecraft\dbpad\help
cd %_DBPAD_HOME%\bin
REM %_JAR_CMD% cvfm %_DBPAD_HOME%\build\dbpad.jar %_DBPAD_HOME%\bin\dbpad.mf -C %_DBPAD_HOME%\src\ %_DBPAD_HOME%\src\com\bluecraft\dbpad\*.class %_DBPAD_HOME%\src\com\bluecraft\dbpad\file\*.class %_DBPAD_HOME%\src\com\bluecraft\dbpad\resource\*.class %_DBPAD_HOME%\src\com\bluecraft\dbpad\util\*.class %_DBPAD_HOME%\src\com\bluecraft\dbpad\images %_DBPAD_HOME%\src\com\bluecraft\dbpad\help
echo Done.
goto cleanup

:doJavadoc
echo Creating API docs...
%_CLEAN_CMD% %_DBPAD_HOME%\docs\api\*.*
%_DOC_CMD% -d %_DBPAD_HOME%\docs\api -sourcepath %_DBPAD_HOME%\src @%_DBPAD_HOME%\bin\package.list
echo Done.
goto cleanup

:doClean
echo Cleaning up...
%_CLEAN_CMD% %_DBPAD_HOME%\src\com\bluecraft\dbpad\*.*~
%_CLEAN_CMD% %_DBPAD_HOME%\src\com\bluecraft\dbpad\*.class
echo Done.
goto cleanup

:doRun
cd %_DBPAD_HOME%
%_RUN_CMD% -DJAVAC=%_JAVAC% com.bluecraft.dbpad.DBPad
REM %_RUN_CMD% -DJAVAC=%_JAVAC% com.bluecraft.dbpad.WorkSheet
cd %_DBPAD_HOME%\bin
goto cleanup

:doStart
cd %_DBPAD_HOME%
%_START_CMD% -DJAVAC=%_JAVAC% com.bluecraft.dbpad.DBPad
REM %_START_CMD% -DJAVAC=%_JAVAC% com.bluecraft.dbpad.WorkSheet
cd %_DBPAD_HOME%\bin
goto cleanup

:doStop
echo Stop is not implemented yet.
goto cleanup

REM ----- Errors --------------------------------------------------------------
:javahomeerror
echo "ERROR: JAVA_HOME not found in your environment."
echo "Please, set the JAVA_HOME variable in your environment to match the"
echo "location of the Java Virtual Machine you want to use."

REM ----- Restore Environment Variables ---------------------------------------
:cleanup
set CLASSPATH=%_CLASSPATH%
set _CLASSPATH=
set _JAVAC=
set _JAVAHELP_LIB=
set _JDBC_LIB=
set _DBPAD_HOME=
set _BUILD_CMD=
set _JAR_CMD=
set _DOC_CMD=
set _CLEAN_CMD=
set _RUN_CMD=
set _START_CMD=
:finish


