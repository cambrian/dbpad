/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WorkSheet.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;


/**
WorkSheet is the main driver class of WorkSheet in Java.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class WorkSheet {
    WsFrame frame = null;
    private Connection con=null;
    
    //Construct the application
    public WorkSheet() {
        // Open database connection here

        // Register all available drivers
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        } catch(Exception exc) {
            exc.printStackTrace();
        }
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            //DriverManager.registerDriver (new oracle.jdbc.driver.OracleDriver());
        } catch(Exception exc) {
            exc.printStackTrace();
        }


        try {
            con = DriverManager.getConnection("jdbc:oracle:thin:@fuzzy:1521:ORCL","fuzzy","fuzzy");

            Statement stmt = con.createStatement();

            ResultSet rset = stmt.executeQuery("SELECT * FROM ALL_TABLES");


            //System.out.println(rset.toString());


            /*
            while (rset.next())
                System.out.println (rset.getString (1));
            */

            
            stmt.close();

            
            //con.close();
        } catch(Exception exc) {
            exc.printStackTrace();
        }

        

        // Create the main frame
        frame = new WsFrame("WorkSheet", con);
        frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    try {
                        frame.closeFrame();
                        System.exit(0);            // close the application
                    } catch(Exception exc) {
                        exc.printStackTrace();
                    }
                }
            });

        Thread wsFrm = new Thread(frame);
        wsFrm.run();
        
        // Show "Splash screen"
        // TODO: This should be run *before* the main frame has been created.
        Thread splash = new Thread(new SplashScreen(frame));
        splash.run();
    }

    protected void finalize() {
        if(con != null) {
            try {
                con.close();
            } catch(Exception exc) {
                exc.printStackTrace();
            }
        }
    }

    // Main method of com.bluecraft.dbpad.WorkSheet.
    // This is the "main" function of WorkSheet class.
    public static void main(String[] args) {
        // Set look-and-feel first
        try  {
            LookFeel.initializeLF();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
                

        // Show connect dialog
        ConnectDialog dialog = new ConnectDialog();
        dialog.showDialogWindow();


        // Starts main application...
        WorkSheet app = new WorkSheet();
    }
}





