/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: CellPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;


/**
CellPanel

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class CellPanel extends JPanel implements AppConstantList, StaticStringList {

    final static public String UNIQUE_NAME = "";
    
    // temporary!!!!
    final static private Random rand = new Random();
    // temporary!!!!

    // This string is used as an identifier.
    // All CellPanel-derived classes should have unique names.
    private String uniqueName = "";
    
    // Parent panel  (IfacePanel)
    public JPanel parentPanel = null;
    
    // Constructors
    public CellPanel() {
        this(UNIQUE_NAME);		
    }
    public CellPanel(String  name) {
        this(name, null);
    }
    public CellPanel(String  name, JPanel parentPanel) {
        // temporary!!!!
        uniqueName = name + rand.nextLong();
        // temporary!!!!
        this.parentPanel = parentPanel;
        
        //Temporary...
        //setBackground(RIGHTPANEL_BG_COLOR);
        setBackground(Color.cyan);
        //
        //setLayout(new BorderLayout());
        add(new JLabel(uniqueName));
        //Temporary...
    }
    
    // Get/set methods
    public String getUniqueName() {
        return uniqueName;
    }
    public void setUniqueName(String  name) {
        uniqueName = name;
    }
    
    public String toString() {
        return getUniqueName();
    }
    
}






