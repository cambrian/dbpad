/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ToolsMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


/**
EdutMenu is a default tools menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ToolsMenu extends JMenu implements AppConstantList, StaticStringList {
        
    boolean bToolBarWithText = false;
    
    protected Action wsAction;
    protected Action optAction;


    //MainFrame topFrame = null;
    MainFrame topFrame = (MainFrame) getTopLevelAncestor();

    ToolsMenu(JFrame topFrame, JToolBar toolBar) {
        this(toolBar);
        this.topFrame = (MainFrame) topFrame;
    }

    ToolsMenu(JToolBar toolBar) {
    
        super("Tools");
        setMnemonic((int)'T');

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;
        ImageIcon imgIcon16 = null;

        // New WorkSheet
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"WorkSheet.gif"));
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"WorkSheet16.gif"));
        wsAction = new AbstractAction("Worksheet", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    WsFrame frame = new WsFrame("WorkSheet");
                    Thread wsFrm = new Thread(frame);
                    wsFrm.run();
                }
            };
        button = toolBar.add(wsAction);
        if(bToolBarWithText) {
            button.setText("Worksheet");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Worksheet");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(wsAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'W');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, Event.CTRL_MASK));

        // Options
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"Options.gif"));
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"Options16.gif"));
        optAction = new AbstractAction("Options", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    //
                }
            };
        button = toolBar.add(optAction);
        if(bToolBarWithText) {
            button.setText("Options");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Options");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(optAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'O');


        // Add space between toolbar groups
        toolBar.addSeparator();
    }

}




