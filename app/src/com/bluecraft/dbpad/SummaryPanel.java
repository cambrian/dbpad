/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: SummaryPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.event.*;
import java.util.*;


/**
SummaryPanel occupies the right-hand side of the three-way split window of AppPanel,
which is the main panel of DBPad in Java.
SummaryPanel displays the DBPad web site.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class SummaryPanel extends JPanel implements StaticStringList {

    JPanel webPane = null;

    // Store the main frame as internal variable
    //private MainFrame topFrame = null;
    private JFrame topFrame = null;
    
    public SummaryPanel() {
        this(null);
    }
    public SummaryPanel(JFrame frame) {
        super();
        //topFrame = (MainFrame) frame;
        topFrame = frame;
        
        //setBackground(Color.yellow);
        //setPreferredSize(new Dimension (200,300));
        
        setLayout(new BorderLayout());
        
        //WebPanel simulates web browser window.
        //Note that this is a temporary solution. We need to use a full web-browser bean here!
        //WebPanel's home page is the DBPad web site.
        webPane = new JPanel();
        webPane.setBackground(Color.yellow);
        add(webPane, BorderLayout.CENTER);
    }
}





