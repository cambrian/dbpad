/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ColorTextPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.net.*;



/**
ColorTextPanel is inherited from JTextPane and implements syntax-coloring.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ColorTextPanel extends JTextPane {

    public ColorTextPanel() {
        super();
        setBackground(new Color(255,255,225));  // ivory color
        
        // Read/Write
        //setEditable(true);



        setEditorKit(new ColorEditorKit());

        
    }


    // For test purposes only
    public static void main(String[] args) {
        LookFeel.initializeLF();
        
        JFrame frame = new JFrame();
        frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    try {
                        // ??????????
                        //getTopLevelAncestor().setVisible(false);    // hide the Frame
                        //getTopLevelAncestor().dispose();            // free the system resources
                        System.exit(0);
                    } catch(Exception exc) {
                        exc.printStackTrace();
                    }
                }
            });

        frame.setSize(new Dimension(450,300));
        frame.setContentPane(new ColorTextPanel());
        frame.setTitle("ColorTextPanel Demo");
        frame.setVisible(true);
    }

}





