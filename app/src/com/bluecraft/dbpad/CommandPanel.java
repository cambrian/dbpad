/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: CommandPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.net.*;



/**
CommandPanel simulates web browser window.
Note that this is a temporary solution. We need to use a full web-browser bean here!
CommandPanel's home page is the DBPad web site.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class CommandPanel extends JTextPane implements AppConstantList, StaticStringList {

    // Store the main frame as internal variable
    //private MainFrame topFrame = null;
    private JFrame topFrame = null;
    
    public CommandPanel() {
        this(null);
    }
    public CommandPanel(JFrame frame) {
        super();
        //topFrame = (MainFrame) frame;
        topFrame = frame;

        setBackground(new Color(255,255,225));  // ivory color
        
        // Read/Write
        setEditable(true);
        

        /*
        // Home page
        String homePage = "file:" + System.getProperty("user.dir") +
                fileSep + "tsplus" + fileSep + "TscHomePage.htm";

        // Set content
        try {
            setPage(homePage);
        } catch(IOException exc) {
            exc.printStackTrace();
        }
        
        // Enable hyperlinks
        addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(HyperlinkEvent e) {
                try {
                    setPage(e.getURL());
                } catch(IOException exc) {
                    exc.printStackTrace();
                }
            }
        });
        */
        
    }


}





