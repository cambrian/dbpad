/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: DBPad.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


/**
DBPad is the main driver class of DBPad in Java.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class DBPad {
    boolean packFrame = true;
    MainFrame frame = null;
    
    //Construct the application
    public DBPad() {
        // Create the main frame
        frame = new MainFrame("DBPad in Java");
        
        //Validate frames that have preset sizes
        //Pack frames that have useful preferred size info, e.g. from their layout
        if (packFrame)
            frame.pack();
        else
            frame.validate();
            
        //Center the window
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = frame.getSize();
        if (frameSize.height > screenSize.height)
            frameSize.height = screenSize.height;
        if (frameSize.width > screenSize.width)
            frameSize.width = screenSize.width;
        frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
        frame.setVisible(true);
                
        // Show "Splash screen"
        // TODO: This should be run *before* the main frame has been created.
        Thread splash = new Thread(new SplashScreen(frame));
        splash.run();
    }


    // Main method of com.bluecraft.dbpad.DBPad.
    // This is the "main" function of DBPad in Java.
    public static void main(String[] args) {
        // Set look-and-feel first
        try  {
            LookFeel.initializeLF();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
                
        // Starts main application...
        DBPad app = new DBPad();
    }
}




