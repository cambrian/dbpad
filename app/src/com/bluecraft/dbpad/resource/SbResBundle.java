/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: SbResBundle.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad.resource;

import java.util.*;


/**
SbResBundle is a resource bundle for StatusBar class.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class SbResBundle extends java.util.ListResourceBundle {
    static final Object[][] contents = {
        { "statusBarText", "Status Bar" },
        { "statusBarIndicator", "Indicator" },
        { "dummyIndicatorToolTip", "Dummy Indicator" },
        { "inSyncIndicatorToolTip", "Synchronizing Status" },
        };

    public Object[][] getContents() {
        return contents;
    }
}


