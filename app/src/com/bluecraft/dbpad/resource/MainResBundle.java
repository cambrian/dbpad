/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: MainResBundle.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad.resource;

import java.util.*;


/**
MainResBundle is a default resource bundle.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class MainResBundle extends java.util.ListResourceBundle {
    static final Object[][] contents = {
        { "exitConfirmMessage", "Do you want to save the work before exit?" }, 
        { "exitConfirmTitle", "DBPad in Java - Exit" }, 
        { "closeConfirmMessage", "Do you want to save the work?" }, 
        { "closeConfirmTitle", "DBPad in Java - Close" }, 
        };

    public Object[][] getContents() {
        return contents;
    }
}



