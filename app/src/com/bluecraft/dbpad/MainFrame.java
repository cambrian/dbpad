/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: MainFrame.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import com.bluecraft.dbpad.file.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.text.*;


/**
MainFrame is the main application frame window of DBPad in Java.
It contains menus, toolbars, and a statusbar.
Main application content should be put inside AppPanel.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class MainFrame extends JFrame implements StaticStringList {
    
    final static boolean DEBUG = false;
    private ResourceBundle RB = ResourceBundle.getBundle("com.bluecraft.dbpad.resource.MainResBundle");
    
    // main application pane!!!
    public AppPanel appPane = null;
    public JPanel mainPane = null;

    // List of clients and services
    public Vector clientList = new Vector();  // Currently saves client/service names only. That is, clientList is a vector of Strings


    // Main application file (currently being "used".
    // But the file remains *unlocked* for most of the time!) 
    public TmpFile mainTmpFile = null;  // It's initially null!!!
    public boolean bModifiedAfterSaved = false;  // Non-modified....
 
    // Application default properties
    Properties appProps = null;
    
    // flags
    public boolean bShowToolBar = true;
    public boolean bToolBarWithText = false;
    public boolean bShowStatusBar = true;
    public boolean bShowCommandPane = true;
    public boolean bShowLogPane = true;
    
    public  JToolBar toolBar = new JToolBar();
    public  StatusBar statusBar = new StatusBar();
    
    FileMenu fileMenu = null;
    EditMenu editMenu = null;
    ViewMenu viewMenu = null;
    ToolsMenu toolsMenu = null;
    HelpMenu helpMenu = null;

    // Currently synchronizing?
    public boolean bSynchronizing = false;
    
    
    
    public MainFrame() {
        this("Application");
    }    

    public MainFrame(String frameTitle) {
        //Do frame stuff.
        super(frameTitle);
	
        // Do nothing when the window frame's close button(x) is pressed.
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                try {
                    MainFrame.this.closeFrame();
                } catch(Exception exc) {
                    exc.printStackTrace();
                    //writeError(exc.toString());
                }
            }
        });


        // set frame window title
        setTitle(frameTitle);
        
        // Set frame window icon
        Image img = null;
        img = Toolkit.getDefaultToolkit().getImage(MainFrame.class.getResource("images"+fileSep+"mainicon.gif"));
        if(img != null) {
            setIconImage(img);
        }
        

        // main application pane!!!
        appPane = new AppPanel(this);  // Pass this main frame as an argument
        

        //Lay out the main pane.
        // mainPane contains Toolbar and appPane
        mainPane = new JPanel();
        mainPane.setLayout(new BorderLayout());
        
        // create and load application default properties
        try {
            // create and load default properties
            //Properties defaultProps = new Properties();
            //FileInputStream in = new FileInputStream("defaultProperties");
            //defaultProps.load(in);
            //in.close();

            // create program properties with default
            //appProps = new Properties(defaultProps);
            appProps = new Properties();

            // now load properties from last invocation
            String userDir = System.getProperty("user.dir");
            //System.err.println(userDir);

            String propertyFileName = userDir + fileSep + "conf" + fileSep + "dbpad.properties";
            FileInputStream in = new FileInputStream(propertyFileName);
            appProps.load(in);
            in.close();
        } catch(IOException exc) {
            exc.printStackTrace();
        }

        // set default size
        //setSize(new Dimension(450, 400));
        //mainPane.setMinimumSize(new Dimension(300, 200));
        String strWidth = appProps.getProperty("MainFrame.Width", "860");
        String strHeight = appProps.getProperty("MainFrame.Height", "560");
        Dimension mainFrameDim = new Dimension(Integer.parseInt(strWidth), Integer.parseInt(strHeight));
        mainPane.setPreferredSize(mainFrameDim);
        
        mainPane.add(appPane, BorderLayout.CENTER);
        mainPane.add(toolBar, BorderLayout.NORTH);
        mainPane.setBorder(BorderFactory.createEtchedBorder());


        //Lay out the content pane.
        //contentPane contains mainPane and statusBar.
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        //contentPane.setPreferredSize(new Dimension(450, 350));
        contentPane.add(mainPane, BorderLayout.CENTER);
        contentPane.add(statusBar, BorderLayout.SOUTH);
        
        // Set content pane
        setContentPane(contentPane);

        
        //Create the toolbar and menu.
        toolBar.setFloatable(true);
        //Make toolbar non-resizable when it's floating
        //toolBar.????.setResizable(false);  // ???????????
        
        fileMenu = new FileMenu(this,toolBar);
        editMenu = new EditMenu(this,toolBar);
        viewMenu = new ViewMenu(this,toolBar);
        toolsMenu = new ToolsMenu(this,toolBar);
        helpMenu = new HelpMenu(this,toolBar);

        //Set up the menu bar.
        JMenuBar mb = new JMenuBar();
        mb.add(fileMenu);
        mb.add(editMenu);
        mb.add(viewMenu);
        mb.add(toolsMenu);
        mb.add(helpMenu);
        setJMenuBar(mb);
        
        // Make it non-resizable... ?????????
        //setResizable(false);
        
        // For look and feel update.
        SwingUtilities.updateComponentTreeUI(this);
        //repaint();

    }

    // finalizer
    // Is this ever called????
    protected void finalize() throws Throwable {
        //
        super.finalize();
    }
    
    public void writeError(String errorText) {
        System.out.println(errorText);
    }
    
        
    //
    public void displayStatus(String statusText) {
        statusBar.setStatus(statusText);
    }
    public void clearStatus() {
        statusBar.setStatus("");
    }


    public void closeFrame() {
        boolean bClose = true;
        if(bModifiedAfterSaved) { 
            // Beep
            Toolkit.getDefaultToolkit().beep();
            // Show a confirmation dialog
            int reply = JOptionPane.showConfirmDialog(this,
                                                    RB.getString("exitConfirmMessage"),
                                                    RB.getString("exitConfirmTitle"),
                                                    JOptionPane.YES_NO_CANCEL_OPTION, 
                                                    JOptionPane.QUESTION_MESSAGE);
            // } //
            // If the confirmation was affirmative, handle exiting.
            if (reply == JOptionPane.YES_OPTION) {
                // Save
                boolean bSaved = false;
                if(mainTmpFile == null) {
                    bSaved = fileMenu.saveToNewFile();
                } else {
                    bSaved = fileMenu.saveToFile(mainTmpFile);
                }
                if(bSaved) {
                    bClose = true;
                } else {
                    // Currently, you cannot end the program unless you save the work
                    // It can be a problem......
                    bClose = false;
                }
            } else if (reply == JOptionPane.NO_OPTION) {
                // Do not save
                //bClose = true;
            } else if (reply == JOptionPane.CANCEL_OPTION) {
                // Do not save
                bClose = false;
            }
        } else {
            // bClose = true;
        }
        
        if(bClose) {
            try {                    
                // Save property settings
                appProps.put("MainFrame.Width", (new Integer(mainPane.getWidth())).toString());
                appProps.put("MainFrame.Height", (new Integer(mainPane.getHeight())).toString());
                
                String userDir = System.getProperty("user.dir");
                String propertyFileName = userDir + fileSep + "conf" + fileSep + "dbpad.properties";
                FileOutputStream out = new FileOutputStream(propertyFileName);
                appProps.store(out, "---DBPad in Java---");
                out.close();

                this.setVisible(false);    // hide the Frame
                this.dispose();            // free the system resources
                System.exit(0);            // close the application
            } catch (Exception exc) {
                exc.printStackTrace();
                writeError(exc.toString());
            }
        }
    }




    // Temporary function
    public void displayNIY() {
        JOptionPane.showMessageDialog(this, 
                                      "Not Implemented Yet", 
                                      "DBPad in Java", 
                                      JOptionPane.INFORMATION_MESSAGE);
    }


    // For test purposes only.
    // Application is the main "driver" class of DBPad
    public static void main(String[] args) {
        LookFeel.initializeLF();
        
        MainFrame frame = new MainFrame();
        frame.setTitle("MainFrame Demo");
        //frame.setSize(450, 350);
        frame.pack();
        frame.setVisible(true);
    }

}






