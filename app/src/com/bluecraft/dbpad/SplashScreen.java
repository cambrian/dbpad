/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: SplashScreen.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;


/**
SplashScreen displays the logo of DBPad in Java at startup.
It is just for fun ;-)

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class SplashScreen extends JWindow implements Runnable, StaticStringList {

    final static String DEFAULT_ABOUT_IMAGE = "images"+fileSep+"splashDBPad.gif";
    // Dismiss it after ABOUT_DURATION mSec
    final static int    ABOUT_DURATION = 5500;  // 5.5 seconds
    
    JLabel imgLabel = null;
    Timer  timer = null;
    
    public SplashScreen() {
        this(null, DEFAULT_ABOUT_IMAGE);
    }
    
    public SplashScreen(Frame frame) {
        this(frame, DEFAULT_ABOUT_IMAGE);
    }
    
    // This is the most commonly used constructor
    /**
    @param frame Main frame window of the application
    @param imgName  Image File for the About Dialog
    */
    public SplashScreen(Frame frame, String imgName) {
        super(frame);

        imgLabel = new JLabel(new ImageIcon(MainFrame.class.getResource(imgName)));
        imgLabel.setBorder(BorderFactory.createRaisedBevelBorder());
        //imgLabel.setPreferredSize(new Dimension(500, 400));

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(imgLabel, BorderLayout.CENTER);
        
        // ABOUT screen stays for ABOUT_DURATION (mSec) by default
        timer = new Timer(ABOUT_DURATION, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Timer timer = (Timer) e.getSource();
                if(SplashScreen.this.timer == timer) {
                    SplashScreen.this.closeSplashScreen();
                }
            }
        });
        
        // When the user clicks this about dialog,
        // it should disappear.
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                SplashScreen.this.closeSplashScreen();
            }
        });

    }
    
    public void showSplashScreen() {
        centerSplashScreen();
        setVisible(true);
    }

    // Center the splash screen
    private void centerSplashScreen() {
        Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension imgSize = imgLabel.getPreferredSize();
        
        setLocation((scrnSize.width/2) - (imgSize.width/2),
                    (scrnSize.height/2) - (imgSize.height/2));
        pack();
    }
    
    private void closeSplashScreen() {
        timer.stop();
        dispose();
        //System.exit(0);  // temporary....
    }

    // SplashScreen uses a separate thread from the main application.
    public void run() {
        showSplashScreen();
        timer.start();
    }
    

    // Copy the following two lines into your program.
    // Normally, before you start your main frame window...
    public static void main(String[] args) {
        Thread sScreen = new Thread(new SplashScreen());
        sScreen.run();
    }   

}




