/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WsEditMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


/**
EdutMenu is a default edit menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class WsEditMenu extends JMenu implements ActionListener, AppConstantList, StaticStringList {
        
    boolean bToolBarWithText = false;
    
    protected Action cutAction;
    protected Action copyAction;
    protected Action pasteAction;
    protected Action findAction;


    //WsFrame topFrame = null;
    WsFrame topFrame = (WsFrame) getTopLevelAncestor();

    WsEditMenu(JFrame topFrame, JToolBar toolBar) {
        this(toolBar);
        this.topFrame = (WsFrame) topFrame;
    }

    WsEditMenu(JToolBar toolBar) {
    
        super("Edit");
        setMnemonic((int)'E');

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;
        ImageIcon imgIcon16 = null;

        // cut
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"cut.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"cut16.gif"));
        cutAction = new AbstractAction("Cut", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    // temporary!!!!!!!!
                    ///////////
                    //topFrame.statusBar.toggleDummyIndicator();
                    //topFrame.statusBar.toggleOnLineIndicator();
                    ///////////
                }
            };
        button = toolBar.add(cutAction);
        if(bToolBarWithText) {
            button.setText("Cut");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Cut");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(cutAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'T');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.CTRL_MASK));

        // Copy
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"copy.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"copy16.gif"));
        copyAction = new AbstractAction("Copy", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                }
            };
        button = toolBar.add(copyAction);
        if(bToolBarWithText) {
            button.setText("Copy");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Copy");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(copyAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'C');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK));
        
        // Paste
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"paste.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"paste16.gif"));
        pasteAction = new AbstractAction("Paste", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                }
            };
        button = toolBar.add(pasteAction);
        if(bToolBarWithText) {
            button.setText("Paste");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Paste");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(pasteAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'P');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK));

        //
        addSeparator();

        // Find
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"find.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"find16.gif"));
        findAction = new AbstractAction("Find...", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                }
            };
        button = toolBar.add(findAction);
        if(bToolBarWithText) {
            button.setText("Find");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Find");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(findAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'F');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.CTRL_MASK));


        // Add space between toolbar groups
        toolBar.addSeparator();
    }


    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Action event detected."
            + "    Event source: " + source.getText();
        //        textArea.append(s + newline);
    }
    
}




