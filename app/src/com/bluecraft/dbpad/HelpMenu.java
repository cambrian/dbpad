/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: HelpMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import javax.help.*;


/**
HelpMenu is a default help menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class HelpMenu extends JMenu implements ActionListener, AppConstantList, StaticStringList {
        
    boolean bToolBarWithText = false;

    protected Action helpContentsAction;
    protected Action helpShooterAction;
    protected Action resourceAction;
    protected Action aboutAction;
    
    HelpSet    hs = null;
    HelpBroker hb = null;
    

    // Store the main frame as internal variable
    private MainFrame topFrame = null;

    HelpMenu(JToolBar toolBar) {
        this(null, toolBar);
    }
        
    HelpMenu(JFrame tFrame, JToolBar toolBar) {

        super("Help");
		setMnemonic((int)'H');
		
		// Set the main frame
		if(tFrame == null) {
		    topFrame = (MainFrame) getTopLevelAncestor();
		} else {
            topFrame = (MainFrame) tFrame;
        }
		

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;
        ImageIcon imgIcon16 = null;


        // Create the help set here.
        try {
            // ???????????????????????
            //URL hsURL = HelpSet.findHelpSet(null, "./help/Template.hs");
            URL hsURL = HelpSet.findHelpSet(null, "com/bluecraft/dbpad/help/Template.hs");

            //System.err.println(hsURL.toString());
            hs = new HelpSet(null, hsURL);
        } catch (Exception exc) {
            System.err.println("HelpSet not found");
            //topFrame.writeError(exc.toString());
            //System.out.println("HelpSet "+helpsetName+" not found");
            //return;
        }

        if(hs != null) {

            hb = hs.createHelpBroker();


        // Help contents
            imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"help.gif"));
            imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"help16.gif"));
            helpContentsAction = new AbstractAction("DBPad Contents", imgIcon) {
                    public void actionPerformed(ActionEvent e) {
                        // Consult JavaHelp documentation!
                        new CSH.DisplayHelpFromSource(hb).actionPerformed(e);
                    }
                };
            button = toolBar.add(helpContentsAction);
            if(bToolBarWithText) {
                button.setText("Help");
            } else {
                button.setText(""); //an icon-only button
            }
            button.setToolTipText("Help Topics");
            button.setMargin(TOOLBARBUTTON_INSETS);
            menuItem = add(helpContentsAction);
            menuItem.setIcon(imgIcon16);
            menuItem.setMnemonic((int)'C');
		
        // Help trouble shooter
            imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"trShooter16.gif"));
            helpShooterAction = new AbstractAction("DBPad Troubleshooter") {
                    public void actionPerformed(ActionEvent e) {
                        troubleShooter_actionPerformed(e);
                    }
                };
            menuItem = add(helpShooterAction);
            menuItem.setIcon(imgIcon16);
            menuItem.setMnemonic((int)'T');
		
	
        //
            addSeparator();
        }		
		
        // On line resources
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"resource.gif"));
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"resource16.gif"));
        resourceAction = new AbstractAction("On-Line Resources", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                onLineResources_actionPerformed(e);
            }
        };
        menuItem = add(resourceAction);
        menuItem.setIcon(imgIcon16);
		menuItem.setMnemonic((int)'R');

        //
        addSeparator();
        

        // About_Box Dialog
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"about16.gif"));
        aboutAction = new AbstractAction("About DBPad...") {
            public void actionPerformed(ActionEvent e) {
                helpAbout_actionPerformed(e);
            }
        };
        menuItem = add(aboutAction);
        menuItem.setIcon(imgIcon16);
		menuItem.setMnemonic((int)'A');


        // Add space between toolbar groups
        //toolBar.addSeparator();
    }



    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Action event detected."
            + "    Event source: " + source.getText();
    }
    
    // Help | Trouble shooter 
    public void troubleShooter_actionPerformed(ActionEvent e) {
        try {
            // temporary
            String shooterPage = new String("http://www.vilab.com/dbpad/");
            openBrowser(shooterPage);
        } catch(Exception exc) {
            exc.printStackTrace();
        }
    }

    // Help | Resource 
    public void onLineResources_actionPerformed(ActionEvent e) {
        try {
            // temporary
            String resourcePage = new String("http://www.vilab.com/dbpad/");
            openBrowser(resourcePage);
        } catch(Exception exc) {
            exc.printStackTrace();
        }
    }
    
    // Help | About
    public void helpAbout_actionPerformed(ActionEvent e) {
        Thread aboutWin = new Thread(new AboutWindow(topFrame));
        aboutWin.run();
    }
    
    
    private boolean openBrowser(String strURL) {
        final String WIN_CMD = "rundll32";
        final String WIN_FLAG = "url.dll,FileProtocolHandler";
    
        String osName = System.getProperty("os.name");
        if ( osName != null && osName.startsWith("Windows")) {
            try {
                String cmd = WIN_CMD + " " + WIN_FLAG + " " + strURL;
                Process p = Runtime.getRuntime().exec(cmd);
            }
            catch(IOException exc)
            {
                System.err.println("Could not open browser");
                exc.printStackTrace();
                return false;
            }
            return true;
        } else {
            System.err.println("Only Windows is supported currently.");
            return false;
        }
    }   
}






