/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WsFrame.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import com.bluecraft.dbpad.file.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.text.*;
import java.sql.*;


/**
WsFrame is the main frame window of WorkSheet class.
It contains menus and toolbars.
Main content should be put inside WsPanel.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class WsFrame extends JFrame implements Runnable, StaticStringList {
    
    final static boolean DEBUG = false;
    private ResourceBundle RB = ResourceBundle.getBundle("com.bluecraft.dbpad.resource.MainResBundle");
    
    // main application pane!!!
    public WsPanel wsPane = null;
    public JPanel mainPane = null;

    // Application default properties
    Properties appProps = null;
    boolean packFrame = true;
    
    // flags
    public boolean bShowToolBar = true;
    public boolean bToolBarWithText = false;

    // Toolbars and menus
    public  JToolBar toolBar = new JToolBar();
    WsFileMenu wsFileMenu = null;
    WsEditMenu wsEditMenu = null;
    WsViewMenu wsViewMenu = null;
    WsQueryMenu wsQueryMenu = null;
    WsHistoryMenu wsHistoryMenu = null;
    WsHelpMenu wsHelpMenu = null;

    // Database
    public Connection connection=null;

    
    public WsFrame() {
        this("Application");
    }    

    public WsFrame(String frameTitle) {
        this(frameTitle, null);
    }
    
    public WsFrame(String frameTitle, Connection con) {
        super(frameTitle);
        connection = con;
	
        // Do nothing when the window frame's close button(x) is pressed.
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    try {
                        WsFrame.this.closeFrame();
                    } catch(Exception exc) {
                        exc.printStackTrace();
                        //writeError(exc.toString());
                    }
                }
            });


        // set frame window title
        setTitle(frameTitle);
        
        // Set frame window icon
        Image img = null;
        img = Toolkit.getDefaultToolkit().getImage(WsFrame.class.getResource("images"+fileSep+"mainicon.gif"));
        if(img != null) {
            setIconImage(img);
        }
        

        // main application pane!!!
        wsPane = new WsPanel(this);  // Pass this main frame as an argument
        

        //Lay out the main pane.
        // mainPane contains Toolbar and wsPane
        mainPane = new JPanel();
        mainPane.setLayout(new BorderLayout());
        
        // create and load application default properties
        try {
            // create and load default properties
            //Properties defaultProps = new Properties();
            //FileInputStream in = new FileInputStream("defaultProperties");
            //defaultProps.load(in);
            //in.close();

            // create program properties with default
            //appProps = new Properties(defaultProps);
            appProps = new Properties();

            // now load properties from last invocation
            String userDir = System.getProperty("user.dir");
            //System.err.println(userDir);

            String propertyFileName = userDir + fileSep + "conf" + fileSep + "dbpad.properties";
            FileInputStream in = new FileInputStream(propertyFileName);
            appProps.load(in);
            in.close();
        } catch(IOException exc) {
            exc.printStackTrace();
        }

        // set default size
        //setSize(new Dimension(450, 400));
        //mainPane.setMinimumSize(new Dimension(300, 200));
        String strWidth = appProps.getProperty("WsFrame.Width", "860");
        String strHeight = appProps.getProperty("WsFrame.Height", "560");
        Dimension WsFrameDim = new Dimension(Integer.parseInt(strWidth), Integer.parseInt(strHeight));
        mainPane.setPreferredSize(WsFrameDim);
        
        mainPane.add(wsPane, BorderLayout.CENTER);
        mainPane.add(toolBar, BorderLayout.NORTH);
        mainPane.setBorder(BorderFactory.createEtchedBorder());


        //Lay out the content pane.
        //contentPane contains mainPane.
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        //contentPane.setPreferredSize(new Dimension(450, 350));
        contentPane.add(mainPane, BorderLayout.CENTER);
        
        // Set content pane
        setContentPane(contentPane);

        
        //Create the toolbar and menu.
        toolBar.setFloatable(false);
        
        wsFileMenu = new WsFileMenu(this,toolBar);
        wsEditMenu = new WsEditMenu(this,toolBar);
        wsViewMenu = new WsViewMenu(this,toolBar);
        wsQueryMenu = new WsQueryMenu(this,toolBar);
        wsHistoryMenu = new WsHistoryMenu(this,toolBar);
        wsHelpMenu = new WsHelpMenu(this,toolBar);

        //Set up the menu bar.
        /*
        JMenuBar mb = new JMenuBar();
        mb.add(wsFileMenu);
        mb.add(wsEditMenu);
        mb.add(wsViewMenu);
        mb.add(wsQueryMenu);
        mb.add(wsHistoryMenu);
        mb.add(wsHelpMenu);
        setJMenuBar(mb);
        */
        
        // Make it non-resizable... ?????????
        //setResizable(false);
        
        // For look and feel update.
        SwingUtilities.updateComponentTreeUI(this);
        //repaint();

    }

    // finalizer
    // Is this ever called????
    protected void finalize() throws Throwable {
        //
        super.finalize();
    }
    
    public void writeError(String errorText) {
        System.out.println(errorText);
    }
    


    private void centerFrame() {
        //Center the window
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = getSize();
        if (frameSize.height > screenSize.height)
            frameSize.height = screenSize.height;
        if (frameSize.width > screenSize.width)
            frameSize.width = screenSize.width;
        setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    }


    public void showFrame() {
        //frame.setSize(450, 350);

        //Validate frames that have preset sizes
        //Pack frames that have useful preferred size info, e.g. from their layout
        if (packFrame)
            pack();
        else
            validate();
        centerFrame();
        setVisible(true);
    }
    
    public void closeFrame() {
        try {                    
            // Save property settings
            appProps.put("WsFrame.Width", (new Integer(mainPane.getWidth())).toString());
            appProps.put("WsFrame.Height", (new Integer(mainPane.getHeight())).toString());
                
            String userDir = System.getProperty("user.dir");
            String propertyFileName = userDir + fileSep + "conf" + fileSep + "dbpad.properties";
            FileOutputStream out = new FileOutputStream(propertyFileName);
            appProps.store(out, "---DBPad in Java---");
            out.close();

            this.setVisible(false);    // hide the Frame
            this.dispose();            // free the system resources
        } catch (Exception exc) {
            exc.printStackTrace();
            writeError(exc.toString());
        }
        //System.exit(0);  // Do not close the application here since WsFrame can be a "dialog window" of the main application.
    }
    
    // Each WorkSheet uses a separate thread from the main application.
    public void run() {
        showFrame();
    }
    

    // Temporary function
    public void displayNIY() {
        JOptionPane.showMessageDialog(this, 
                                      "Not Implemented Yet", 
                                      "DBPad in Java", 
                                      JOptionPane.INFORMATION_MESSAGE);
    }


    // For test purposes only.
    // Application is the main "driver" class of DBPad
    public static void main(String[] args) {
        LookFeel.initializeLF();
        
        WsFrame frame = new WsFrame();
        frame.setTitle("WsFrame Demo");
        Thread aWin = new Thread(frame);
        aWin.run();
    }

}






