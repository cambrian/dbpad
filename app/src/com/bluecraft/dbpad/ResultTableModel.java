/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ResultTableModel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;

import java.io.IOException;
import java.io.File;
import java.util.Date;
import java.util.Stack;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreePath;
import javax.swing.table.*;


/**
ResultTableModel
                
@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ResultTableModel extends DefaultTableModel {
    
    private static String[] columnNames = {"Interface",
                                           "WAN Protocol",
                                           "IP Address",
                                           "Routing Functions",
                                           "Access List"};
                                           
    public static final int SORT_ASCENDING = 1;
    public static final int SORT_NOTSORTED = 0;
    public static final int SORT_DESCENDING = -1;
    private int[] sortOrder = {SORT_NOTSORTED,
                               SORT_NOTSORTED,
                               SORT_NOTSORTED,
                               SORT_NOTSORTED,
                               SORT_NOTSORTED};
                               
    public ResultTableModel() { 
	    this(columnNames, 0);
    }
    public ResultTableModel(String[] colNames, int numRows) { 
	    super(colNames, numRows);
	    
        /*
        // temporary
        for(int k=0;k<5;k++) {
            CellClass[] columnObjects = {new ResultClass("ResultClass" + k),
                                           new  WanProtocol("WanProtocol" + (k+1)%5),
                                           new  IpAddress("IpAddress" + (k+2)%5),
                                           new  RouteFunction("RouteFunction" + (k+3)%5),
                                           new  AccessList("AccessList" + (k+4)%5)};
            addRow(columnObjects);
        }
        // temporary
        */
    }


    public boolean isCellEditable(int row, int col) {
        // temporary.....
        return false;
    }


    public int sortRow(int col, int selRow) {
        /*
        if(sortOrder[col] == SORT_DESCENDING || sortOrder[col] == SORT_NOTSORTED) {
            sortOrder[col] = SORT_ASCENDING;
            return sortAscending(col, selRow);
        } else if(sortOrder[col] == SORT_ASCENDING) {
            sortOrder[col] = SORT_DESCENDING;
            return sortDescending(col, selRow);
        }
        //fireTableStructureChanged();
        //System.out.println("Sort order: " + sortOrder[col]);
        */
        return 0;
    }
    
    /*
    private int sortAscending(int col, int selRow) {
        int rCount = getRowCount();
        for(int i=0;i<rCount;i++) {
            CellClass iVal = (CellClass) getValueAt(i, col);
            for(int j=i+1;j<rCount;j++) {
                CellClass jVal = (CellClass) getValueAt(j, col);
                ///
                if(iVal.compareTo(jVal) > 0) {
                    swap(i, j, iVal, jVal);
                    if(i == selRow) {
                        selRow = j;
                    } else if(j == selRow) {
                        selRow = i;
                    }   
                }
                ///
            }
        }
        //System.out.println("sorting....");
        return selRow;
    }
    */

    /*
    private int sortDescending(int col, int selRow) {
        int rCount = getRowCount();
        for(int i=0;i<rCount;i++) {
            CellClass iVal = (CellClass) getValueAt(i, col);
            for(int j=i+1;j<rCount;j++) {
                CellClass jVal = (CellClass) getValueAt(j, col);
                ///
                if(iVal.compareTo(jVal) < 0) {
                    swap(i, j, iVal, jVal);
                    if(i == selRow) {
                        selRow = j;
                    } else if(j == selRow) {
                        selRow = i;
                    }   
                }
                ///
            }
        }
        //System.out.println("sorting....");
        return selRow;
    }
    */
    
    /*
    private void swap(int i, int j, CellClass iVal, CellClass jVal) {
        moveRow(j,j,i);
        moveRow(i+1,i+1,j);
        iVal = jVal;
    }
    */
}



