/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WsHistoryMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import com.bluecraft.dbpad.file.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.text.*;
import java.io.*;
import java.util.*;


/**
WsHistoryMenu is a default history menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class WsHistoryMenu extends JMenu implements AppConstantList, StaticStringList {
        
    ResourceBundle RB = ResourceBundle.getBundle("com.bluecraft.dbpad.resource.MainResBundle");

    boolean bToolBarWithText = false;
    
    Action upAction;
    Action downAction;


    //WsFrame topFrame = null;
    WsFrame topFrame = (WsFrame) getTopLevelAncestor();

    WsHistoryMenu(JFrame topFrame, JToolBar toolBar) {
        this(toolBar);
        this.topFrame = (WsFrame) topFrame;
    }

    WsHistoryMenu(JToolBar toolBar) {
        
        super("History");
        setMnemonic((int)'H');

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;
        ImageIcon imgIcon16 = null;


        // Up
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"HistoryUp.gif"));
        upAction = new AbstractAction("History Up", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    //
                    if(topFrame != null) {
                        WsPanel wsPane = (WsPanel) topFrame.wsPane;
                        wsPane.getPreviousQuery();
                    }
                    //
                }
            };
        button = toolBar.add(upAction);
        if(bToolBarWithText) {
            button.setText("Up");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("History Up");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(upAction);
        menuItem.setIcon(null);
        menuItem.setMnemonic((int)'U');
        upAction.setEnabled(false);   // Initially disabled!

        // Down
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"HistoryDown.gif"));
        downAction = new AbstractAction("History Down", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    //
                    if(topFrame != null) {
                        WsPanel wsPane = (WsPanel) topFrame.wsPane;
                        wsPane.getNextQuery();
                    }
                    //
                }
            };
        button = toolBar.add(downAction);
        if(bToolBarWithText) {
            button.setText("Down");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("History Down");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(downAction);
        menuItem.setIcon(null);
        menuItem.setMnemonic((int)'U');
        downAction.setEnabled(false);   // Initially disabled!

        
        // Add space between toolbar groups
        toolBar.addSeparator();
    }

}




