/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ProjectNode.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;


/**
ProjectNode is the parent node of all node types of the ProjectTree.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ProjectNode extends DefaultMutableTreeNode implements StaticStringList {

    final static private int DEFAULT_NODETYPE = NODETYPE_UNKNOWN;
    final static private String DEFAULT_NODETEXT = " ";
    private int nodeType = -1;

    public ProjectNode() {
        this(DEFAULT_NODETYPE);
    }
    public ProjectNode(int ndType) {
        this(DEFAULT_NODETEXT, ndType);
    }
    public ProjectNode(String nodeText) {
        this(nodeText, DEFAULT_NODETYPE);
    }
    public ProjectNode(String nodeText, int ndType) {
        super(nodeText);
        nodeType = ndType;
    }
    
    // Get/Set methods
    public int getNodeType() {
        return nodeType;
    }
    public void setNodeType(int ndType) {
        nodeType = ndType;
    }
    
}




