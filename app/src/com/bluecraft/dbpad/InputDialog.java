/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: InputDialog.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;


/**
InputDialog is a superclass of (almost) all dialog windows which requre user's inputs.
Its contentPane consists of upper mainPane, which contains input window,
and lower bottomPane, which contains default buttons.
InputDialog is *declared* to be an abstract class, and cannot be instantiated.
Please derive a subclass.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public abstract class InputDialog extends JDialog implements StaticStringList {

    final protected Color  BG_COLOR = Color.lightGray;

    JPanel mainPane = new JPanel();
    JPanel topPane = new JPanel();
    JPanel bottomPane = new JPanel();
    
    
    protected JButton okButton = new JButton("OK");
    protected JButton cancelButton = new JButton("Cancel");
    protected JButton helpButton = new JButton("Help");

    protected JFrame topFrame = null;


    public InputDialog() {
        this(null);
    }
    
    public InputDialog(JFrame frame) {
        super(frame, "DBPad", true);  // Always modal!!!!

        if(frame instanceof MainFrame) {
            topFrame = (MainFrame) frame;
        } else if(frame instanceof WsFrame) {
            topFrame = (WsFrame) frame;
        } else {
            topFrame = frame;
        }
            
        // Just hide it when the dialog's close button(x) is pressed.
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        // Make it non-resizable!!!
        setSize(new Dimension(320,280));
        setResizable(false);
        
        // TODO: How to set icon for JDialog????????????????????????
        //if(topFrame != null) {
        //    ?setIconImage?(topFrame.getIconImage());
        //}
        
        
        // Content pane = main pane + bottom pane
        Container contentPane = getContentPane();
        contentPane.setBackground(BG_COLOR);
        contentPane.setLayout(new BorderLayout());
        
        
        // main pane
        mainPane.setBackground(BG_COLOR);
        //mainPane.setLayout(new BorderLayout());  // Do *not* set mainPane's layout here!
		
        topPane.setBackground(BG_COLOR);
	topPane.setBorder(new EmptyBorder(5,5,5,5)); 
        topPane.add(mainPane, BorderLayout.CENTER);

		
        // Initial states of buttons
        okButton.setEnabled(true);
        cancelButton.setEnabled(true);
        helpButton.setEnabled(true);
        
        
        // Button action listners
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Do something...
                
                // hide the dialog
                //TabbedDialog.this.setVisible(false);
                hideDialogWindow();
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // hide the dialog (do not dismiss it)
                //TabbedDialog.this.setVisible(false);
                hideDialogWindow();
            }
        });
        helpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Bring up help window (context-sensitive)
                
            }
        });
        
        // bottom pane
        bottomPane.setBackground(BG_COLOR);
        bottomPane.setLayout(new BorderLayout());
        
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout());
        buttonPane.setBackground(BG_COLOR);
        okButton.setPreferredSize(new Dimension(75,26));
        cancelButton.setPreferredSize(new Dimension(75,26));
        helpButton.setPreferredSize(new Dimension(75,26));
        buttonPane.add(okButton);
        buttonPane.add(cancelButton);
        buttonPane.add(helpButton);
        bottomPane.add(buttonPane, BorderLayout.CENTER);
		
        bottomPane.setBorder(new EmptyBorder(0,5,5,5));  // No top margin
		
        contentPane.add(topPane, BorderLayout.CENTER);
        contentPane.add(bottomPane, BorderLayout.SOUTH);
        
    }

    
    // Center the dialog window relative to the main frame window
    private void centerDialogWindow() {
        Dimension dlgSize = getSize();
        
        if(topFrame != null) {
            Point frameLocation = topFrame.getLocation();
            Dimension frameSize = topFrame.getSize();
            setLocation((frameLocation.x + frameSize.width/2) - (dlgSize.width/2),
                        (frameLocation.y + frameSize.height/2) - (dlgSize.height/2));
        } else {
            Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((scrnSize.width/2) - (dlgSize.width/2),
                        (scrnSize.height/2) - (dlgSize.height/2));
        }
    }

    public void showDialogWindow() {
        centerDialogWindow();
        setVisible(true);
    }
    public void hideDialogWindow() {
        // hide the dialog (do not dismiss it)
        setVisible(false);
        //dispose();
    }

    ////////////////////////////////////////
    public static void main(String[] args) {
        InputDialog dialog = new InputDialog() {
            // This is trick. InputDialog itself is an abstract class and cannot be instantiated.
        };
        //dialog.setSize(new Dimension(450,350));
        /* //////////////////////////////////////////////////
        dialog.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);            // close the application
            }
        });
        */ //////////////////////////////////////////////////
        dialog.showDialogWindow();
    }
    ////////////////////////////////////////

}

 



