/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: QueryPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.io.*;
import java.net.*;



/**
QueryPanel 

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class QueryPanel extends ColorTextPanel implements AppConstantList, StaticStringList {

    // Store the main frame as internal variable
    //private WsFrame topFrame = null;
    private JFrame topFrame = null;
    
    public QueryPanel() {
        this(null);
    }
    public QueryPanel(JFrame frame) {
        super();
        //topFrame = (WsFrame) frame;
        topFrame = frame;

        setBackground(new Color(255,255,225));  // ivory color
        
        // Read/Write
        setEditable(true);

        // Should save and read the queryPane size from property files.  
        setPreferredSize(new Dimension(150,150));

        
    }



}





