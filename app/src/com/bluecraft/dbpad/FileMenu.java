/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: FileMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import com.bluecraft.dbpad.file.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.text.*;
import java.io.*;
import java.util.*;


/**
FileMenu is a default file menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class FileMenu extends JMenu implements ActionListener, 
    AppConstantList, StaticStringList {
        
    ResourceBundle RB = ResourceBundle.getBundle("com.bluecraft.dbpad.resource.MainResBundle");

    boolean bToolBarWithText = false;
    
    Action newFileAction;
    Action openFileAction;
    Action saveFileAction;
    Action saveAsAction;
    Action printSetupAction;
    Action printAction;
    Action exitAction;
    
    private JFileChooser fileChooser = null;


    //MainFrame topFrame = null;
    MainFrame topFrame = (MainFrame) getTopLevelAncestor();

    FileMenu(JFrame topFrame, JToolBar toolBar) {
        this(toolBar);
        this.topFrame = (MainFrame) topFrame;
    }

    FileMenu(JToolBar toolBar) {
        
        //JMenu fileMenu = new JMenu("File");
        super("File");
		setMnemonic((int)'F');
		
		// Create file chooser
        fileChooser = new JFileChooser();
        // Set the default directory
        // TODO: Save and load the directory the user used last time.
        File defaultDir = new File(System.getProperty("user.dir"));
        fileChooser.setCurrentDirectory(defaultDir);
        // Set file filters here
        fileChooser.setFileFilter(new TmpFileFilter());
        //fileChooser.addChoosableFileFilter(new TmpFileFilter());
		

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;
        ImageIcon imgIcon16 = null;

        // New file
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"new.gif"));
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"new16.gif"));
        newFileAction = new AbstractAction("New", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                //
                //
                // (1) Save and close the old tmp file
                boolean bResult = cleanUp();
                
                // (2) Then set 
                if(bResult) {
                    topFrame.mainTmpFile = null;
                    topFrame.bModifiedAfterSaved = false;
                    //topFrame.writeLog("Created a new tmp file");
                }
                //
                //
            }
        };
        button = toolBar.add(newFileAction);
        if(bToolBarWithText) {
            button.setText("New");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("New File");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(newFileAction);
        menuItem.setIcon(imgIcon16);
		menuItem.setMnemonic((int)'N');
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Event.CTRL_MASK));

        // Open file
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"open.gif"));
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"open16.gif"));
        openFileAction = new AbstractAction("Open...", imgIcon) {
            
            public void actionPerformed(ActionEvent e) {

                // (0) Ask whether to close the existing file (if there's one)
                // (1) Save and close the old tmp file
                boolean bResult = cleanUp();
                
                if(bResult) {
                    topFrame.bModifiedAfterSaved = false;

                    //  Then ask for a new file name to be open
                    fileChooser.setDialogTitle("Open File");
                    int state = fileChooser.showOpenDialog(topFrame);
                    File file = fileChooser.getSelectedFile();
                    
                    if(file != null && state == JFileChooser.APPROVE_OPTION) {
                        TmpFile tmpile = new TmpFile(file.getPath());

                        // ????????????????
                        boolean bSuccessful = readFromFile(tmpile);
                        if(bSuccessful) {
                            // operation successful
                            topFrame.mainTmpFile = tmpile;
                            //topFrame.writeLog("Opened an tmp file " + tmpile.toString());
                        } else {
                            // something's wrong....
                            //topFrame.writeLog("Failed to open an tmp file " + tmpile.toString());
                        }
                    } else {
                        // operation canceled.
                    }
                } else {
                    // Didn't close the old file
                }
            }
        };
        button = toolBar.add(openFileAction);
        if(bToolBarWithText) {
            button.setText("Open");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Open File");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(openFileAction);
        menuItem.setIcon(imgIcon16);
		menuItem.setMnemonic((int)'O');
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Event.CTRL_MASK));
        
        // Save file
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"save.gif"));
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"save16.gif"));
        saveFileAction = new AbstractAction("Save", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                
                boolean bResult = false;
                if(topFrame.mainTmpFile == null) {
                    bResult = saveToNewFile();
                }  else {
                    bResult = saveToFile(topFrame.mainTmpFile);
                }
                
                // ????????????????
                if(bResult) {
                    // operation successful
                    topFrame.bModifiedAfterSaved = false;                
                } else {
                    // something's wrong....
                }
                
            }
        };
        // oops. topFrame is still null............
        //saveFileAction.setEnabled(topFrame.bModifiedAfterSaved);
        
        button = toolBar.add(saveFileAction);
        if(bToolBarWithText) {
            button.setText("Save");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Save File");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(saveFileAction);
        menuItem.setIcon(imgIcon16);
		menuItem.setMnemonic((int)'S');
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));
        
        // Save As...
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"saveAs.gif"));
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"saveAs16.gif"));
        saveAsAction = new AbstractAction("Save As...", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                boolean bResult = saveToNewFile();

                // ????????????????
                if(bResult) {
                    // operation successful
                    topFrame.bModifiedAfterSaved = false;                
                } else {
                    // something's wrong....
                }
                
            }
        };
        /*
        button = toolBar.add(saveAsAction);
        if(bToolBarWithText) {
            button.setText("Save As");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Save File with New Name");
        button.setMargin(TOOLBARBUTTON_INSETS);
        */
        menuItem = add(saveAsAction);
        menuItem.setIcon(imgIcon16);
		menuItem.setMnemonic((int)'A');

		//
        addSeparator();

        // Print setup
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"printSetup.gif"));
        imgIcon16 = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"printSetup16.gif"));
        printSetupAction = new AbstractAction("Print Setup...", imgIcon) {
            public void actionPerformed(ActionEvent e) {
            }
        };
        /*
        button = toolBar.add(printSetupAction);
        if(bToolBarWithText) {
            button.setText("Print Setup");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Print Setup");
        button.setMargin(TOOLBARBUTTON_INSETS);
        */
        menuItem = add(printSetupAction);
        menuItem.setIcon(null);
		menuItem.setMnemonic((int)'U');

        // Print
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"print.gif"));
        printAction = new AbstractAction("Print", imgIcon) {
            public void actionPerformed(ActionEvent e) {
            }
        };
        button = toolBar.add(printAction);
        if(bToolBarWithText) {
            button.setText("Print");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Print");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(printAction);
        menuItem.setIcon(null);
		menuItem.setMnemonic((int)'P');
		menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, Event.CTRL_MASK));
		
		//
        addSeparator();

        // Exit
        imgIcon = new ImageIcon(MainFrame.class.getResource("images"+fileSep+"exit.gif"));
        exitAction = new AbstractAction("Exit", imgIcon) {
            public void actionPerformed(ActionEvent e) {
                ((MainFrame) topFrame).closeFrame();
            }
        };
        /*
        button = toolBar.add(exitAction);
        if(bToolBarWithText) {
            button.setText("Exit");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Exit");
        */
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(exitAction);
        menuItem.setIcon(null);
		//menuItem.setHorizontalTextPosition(SwingConstants.RIGHT);
		menuItem.setMnemonic((int)'X');

        
        // Add space between toolbar groups
        toolBar.addSeparator();
    }


    //
    public boolean saveToNewFile() {
        
        fileChooser.setDialogTitle("Save As...");
        int state = fileChooser.showSaveDialog(topFrame);
        File file = fileChooser.getSelectedFile();
        
        boolean bResult = false;
        if(file != null && state == JFileChooser.APPROVE_OPTION) {
            TmpFile tmpile = new TmpFile(file.getPath());
            // TODO: Append ".tmp" if the file doesn't have an extension!
            
            //
            bResult = saveToFile(tmpile);
            if(bResult) {
                //topFrame.mainTmpFile = tmpile;
                //topFrame.bModifiedAfterSaved = false;
                //topFrame.writeLog("Saved to " + tmpile.toString());
            } else {
                // something's wrong....
            }
            
        } else {
            // operation canceled.
        }
        
        return bResult;
    }


    public boolean saveToFile(TmpFile file) {
        boolean bSuccessful = false;
                    
        // [1] Open the file
        
        if(file == null) {
            // something's wrong. 
            System.err.println("Source path name not valid");
        }


        // [2] Save the data here.
        
        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new FileWriter(file)));
                    
            topFrame.mainTmpFile = file;
            topFrame.bModifiedAfterSaved = false;
            //topFrame.writeLog("Saved to " + file.toString());

            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + file.getPath());
            //topFrame.writeError(exc.toString());
        } catch(IOException exc) {
            exc.printStackTrace();
            //topFrame.writeError(exc.toString());
        }
        
        // temporary
        //JOptionPane.showMessageDialog(topFrame, "Tmp File " + file.getPath() + " saved!");
        
               
        // Temporary.....        
        bSuccessful = true;
        return true;
    }

    private boolean readFromFile(TmpFile file) {
        boolean bSuccessful = false;
        
        // [1] "Parse" Tmp file!!!!
        //DomParserFactory.setParser(IBM_PARSER);
        //Document doc = DomParserFactory.openLocalDocument(file.getPath());
        
        Document doc = null;
        if(doc != null) {
            
            /////
            topFrame.bModifiedAfterSaved = false;
            
            // temporary
            bSuccessful = true;
        } else {
            bSuccessful = false;
        }
        
        return bSuccessful;
     }
    
    private boolean cleanUp()
    {
        // save first
        boolean bGoAhead = true;
        if(topFrame.bModifiedAfterSaved) {
            int reply = JOptionPane.showConfirmDialog(topFrame,
                                                    RB.getString("closeConfirmMessage"),
                                                    RB.getString("closeConfirmTitle"),
                                                    JOptionPane.YES_NO_CANCEL_OPTION, 
                                                    JOptionPane.WARNING_MESSAGE);
            if (reply == JOptionPane.YES_OPTION) {
                if(topFrame.mainTmpFile == null) {
                    bGoAhead = saveToNewFile();
                } else {
                    bGoAhead = saveToFile(topFrame.mainTmpFile);
                }
            } else if (reply == JOptionPane.CANCEL_OPTION) {
                bGoAhead = false;
            } else {
                //if (reply == JOptionPane.NO_OPTION)
                // do not save, but go ahead and clean up
            }
        }  // else bGoAhead == true
        
        // then
        if(bGoAhead) {
            // (1) close the old file
            if(topFrame.mainTmpFile != null) {
                //topFrame.writeLog("Closed Tmp file " + topFrame.mainTmpFile.toString());
            }
            
            
            // (3) Remove all associated panels on RightPanel
            // and also clean up default panels....
            //
            
            return true;
        } else {
            //
            return false;
        }
    }
    
    
    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Action event detected."
            + "    Event source: " + source.getText();
        //        textArea.append(s + newline);
    }
    
}




