/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ProjectTreeCellRenderer.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.event.*;
import java.util.*;


/**
ProjectTreeCellRenderer is a default cell renderer of ProjectTree.
It displays different icons depending on the node-type of ProjectNode.
Otherwise, it has the identical functions as DefaultTreeCellRenderer.
                
@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ProjectTreeCellRenderer extends DefaultTreeCellRenderer 
    implements StaticStringList {

    static private Icon  unknownIcon = 
        new ImageIcon(ProjectTreeCellRenderer.class.getResource("images"+fileSep+"unknownNode.gif"));
    static private Icon  homeIcon = 
        new ImageIcon(ProjectTreeCellRenderer.class.getResource("images"+fileSep+"homeNode.gif"));
    static private Icon  commentIcon = 
        new ImageIcon(ProjectTreeCellRenderer.class.getResource("images"+fileSep+"commentNode.gif"));
    static private Icon  warningIcon = 
        new ImageIcon(ProjectTreeCellRenderer.class.getResource("images"+fileSep+"warningNode.gif"));
    static private Icon  successIcon = 
        new ImageIcon(ProjectTreeCellRenderer.class.getResource("images"+fileSep+"successNode.gif"));
    static private Icon  failureIcon = 
        new ImageIcon(ProjectTreeCellRenderer.class.getResource("images"+fileSep+"failureNode.gif"));
    static private Icon  failsuccIcon = 
        new ImageIcon(ProjectTreeCellRenderer.class.getResource("images"+fileSep+"failsuccNode.gif"));
    static private Icon  hourglassIcon = 
        new ImageIcon(ProjectTreeCellRenderer.class.getResource("images"+fileSep+"hourglassNode.gif"));
        
        
    //public ProjectTreeCellRenderer() {
    //    super();
    //}
    
    
    // DefaultTreeCellRenderer's getTreeCellRendererComponent() is overwritten here
    // in order to have different icon drawing schemes!!!!
    public Component getTreeCellRendererComponent(JTree tree, Object value,
						  boolean sel,
						  boolean expanded,
						  boolean leaf, int row,
						  boolean hasFocus) {
	    String         stringValue = tree.convertValueToText(value, sel,
					    expanded, leaf, row, hasFocus);

	    setEnabled(tree.isEnabled());
	    //this.hasFocus = hasFocus;  // ?????
	    setText(stringValue);
	    if(sel)
	        setForeground(getTextSelectionColor());
	    else
	        setForeground(getTextNonSelectionColor());
	        
       	if(value instanceof ProjectNode) {
       	    ProjectNode projectNode = (ProjectNode) value;
       	    switch(projectNode.getNodeType()) {
       	    case NODETYPE_UNKNOWN:
       	        setIcon(unknownIcon);
       	        break;
       	    case NODETYPE_COMMENT:
       	        setIcon(commentIcon);
       	        break;
       	    case NODETYPE_WARNING:
       	        setIcon(warningIcon);
       	        break;
       	    case NODETYPE_HOME:
       	        setIcon(homeIcon);
       	        break;
       	    case NODETYPE_SUCCESS:
       	        setIcon(successIcon);
       	        break;
       	    case NODETYPE_FAILURE:
       	        setIcon(failureIcon);
       	        break;
       	    case NODETYPE_FAILSUCC:
       	        setIcon(failsuccIcon);
       	        break;
       	    case NODETYPE_HOURGLASS:
       	        setIcon(hourglassIcon);
       	        break;
       	    default:
       	        // Something's wrong. Just use the default icon.
       	        setIcon(unknownIcon);
       	        break;
       	    }
    	} else {
    	    // default icon is used
    	    setIcon(unknownIcon);
    	    /*
	        if (leaf) {
	            setIcon(getLeafIcon());
	        } else if (expanded) {
	            setIcon(getOpenIcon());
	        } else {
	            setIcon(getClosedIcon());
	        }
	        */
    	}
    	
	    selected = sel;
	    return this;
    }
}




