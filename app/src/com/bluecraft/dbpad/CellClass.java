/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: CellClass.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;


/**
CellClass is a superclass of all classes associated with cells of ResultTable.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class CellClass implements Comparable {
    
    final static public String DEFAULT_CELLCLASSNAME = "";
    
    protected String cellClassName = null;
   
    ////////////////////
    public  CellPanel  cellPane = null;
    ////////////////////
    
    public CellClass() {
        this(DEFAULT_CELLCLASSNAME);
    }
    public CellClass(String cellClassName) {
        if(cellClassName.trim().length() == 0) {
            this.cellClassName = DEFAULT_CELLCLASSNAME;
        } else {
            this.cellClassName = cellClassName;
        }
        
    }
  
    public String toString() {
        // Just return the name of the object
        return cellClassName;
    }
   
   
    public int compareTo(Object obj) {
        
        ///////
        // Compare just names
        return toString().compareTo(obj.toString());
        ///////
    }

    /////
    public void addPanel(JPanel parentPanel) {
        if(parentPanel == null) {
            // error
            return;
        }
        /*
        JPanel card = ((IfacePanel) parentPanel).cardPane;
        if(card == null) {
            // error
            System.out.println("Card Panel is null: Cannot add panels");
            return;
        }
        card.add(cellPane.getUniqueName(), cellPane);
       /////
       //System.out.println(cellPane.getUniqueName());
        */
    }
       
    public void showPanel(JPanel parentPanel) {
        /*
       JPanel card = ((IfacePanel) parentPanel).cardPane;
       if(card == null) {
            // error
            System.out.println("Card Panel is null");
            return;
       }
       CardLayout cl = (CardLayout)(card.getLayout());
       cl.show(card, cellPane.getUniqueName());
       parentPanel.revalidate();
       parentPanel.repaint();
       /////
       System.out.println(cellPane.getUniqueName());
        */
    }  
    
}





