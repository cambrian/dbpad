<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE helpset
  PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 1.0//EN"
         "http://java.sun.com/products/javahelp/helpset_1_0.dtd">

<?MyFavoriteApplication this is data for my favorite application ?>

<helpset version="1.0">

  <!-- title -->
  <title>Application Template</title>

  <!-- maps -->
  <maps>
     <homeID>template_intro</homeID>
     <mapref location="TemplateMap.jhm"/>
  </maps>

  <!-- views -->
  <view>
    <name>TOC</name>
    <label>Application Template</label>
    <type>javax.help.TOCView</type>
    <data>TemplateTOC.xml</data>
  </view>

  <view>
    <name>Index</name>
    <label>Index</label>
    <type>javax.help.IndexView</type>
    <data>TemplateIndex.xml</data>
  </view>

  <view>
    <name>Search</name>
    <label>Search</label>
    <type>javax.help.SearchView</type>
    <data engine="com.sun.java.help.search.DefaultSearchEngine">
      JavaHelpSearch
    </data>
  </view>

</helpset>

