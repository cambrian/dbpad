/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WsQueryMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


/**
EdutMenu is a default Query menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class WsQueryMenu extends JMenu implements AppConstantList, StaticStringList {
        
    boolean bToolBarWithText = false;
    
    protected Action runAction;
    protected Action parseAction;
 

    //WsFrame topFrame = null;
    WsFrame topFrame = (WsFrame) getTopLevelAncestor();

    WsQueryMenu(JFrame topFrame, JToolBar toolBar) {
        this(toolBar);
        this.topFrame = (WsFrame) topFrame;
    }

    WsQueryMenu(JToolBar toolBar) {
    
        super("Query");
        setMnemonic((int)'Q');

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;
        ImageIcon imgIcon16 = null;

        // Parse
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"QueryParse.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"QueryParse16.gif"));
        parseAction = new AbstractAction("Query Parse", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    if(topFrame != null) {
                        WsPanel wsPane = (WsPanel) topFrame.wsPane;
                        wsPane.parseQuery();
                    }
                }
            };
        button = toolBar.add(parseAction);
        if(bToolBarWithText) {
            button.setText("Parse");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Query Parse");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(parseAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'P');

        // Run
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"QueryRun.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"QueryRun16.gif"));
        runAction = new AbstractAction("Query Run", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    if(topFrame != null) {
                        WsPanel wsPane = (WsPanel) topFrame.wsPane;
                        wsPane.runQuery();
                    }
                }
            };
        button = toolBar.add(runAction);
        if(bToolBarWithText) {
            button.setText("Run");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Query Run");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(runAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'R');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Event.CTRL_MASK));


        // Add space between toolbar groups
        toolBar.addSeparator();
    }

}




