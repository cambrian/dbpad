/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ResultPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.net.*;


/**
ResultPanel occupies the bottom area of WorkSheet,
and it displays the resultset of SQL queries.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ResultPanel extends JPanel implements AppConstantList {
    
    //ResultTable       resultTable = null;

    public ResultPanel() {
        
        setLayout(new BorderLayout());
        //setBackground(RIGHTPANEL_BG_COLOR);
        //setPreferredSize(RIGHTPANEL_PRF_DIMENSION);
        //setMinimumSize(RIGHTPANEL_MIN_DIMENSION);
        setBackground(Color.white);
        setMinimumSize(new Dimension(400,100));
        setPreferredSize(new Dimension(400,150));
		
        //////////////
        //ResultTableModel resultModel = new ResultTableModel();
        //resultTable = new ResultTable(resultModel, this);
        JTable resultTable = new JTable(new DefaultTableModel());
        ///////////////

        // Table for Query Result
        JScrollPane sPane = new JScrollPane(resultTable);
        sPane.setBorder(BorderFactory.createEmptyBorder(25,25,25,25));
        add(sPane, BorderLayout.CENTER);
        add(Box.createVerticalStrut(1), BorderLayout.NORTH);
        add(Box.createVerticalStrut(5), BorderLayout.SOUTH);
        add(Box.createHorizontalStrut(5), BorderLayout.WEST);
        add(Box.createHorizontalStrut(5), BorderLayout.EAST);
    }
    
    ////////////////////////////////////////
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setContentPane(new ResultPanel());
        frame.setSize(new Dimension(600,500));
        //frame.pack();
        frame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);            // close the application
                }
            });
        frame.setVisible(true);
    }
    ////////////////////////////////////////
}




