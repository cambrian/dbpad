/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WsHelpMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import javax.help.*;


/**
WsHelpMenu is a default help menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class WsHelpMenu extends JMenu implements ActionListener, AppConstantList, StaticStringList {
        
    boolean bToolBarWithText = false;

    protected Action helpContentsAction;
    protected Action aboutAction;
    
    HelpSet    hs = null;
    HelpBroker hb = null;
    

    // Store the main frame as internal variable
    private WsFrame topFrame = null;

    WsHelpMenu(JToolBar toolBar) {
        this(null, toolBar);
    }
        
    WsHelpMenu(JFrame tFrame, JToolBar toolBar) {

        super("Help");
        setMnemonic((int)'H');
		
        // Set the main frame
        if(tFrame == null) {
            topFrame = (WsFrame) getTopLevelAncestor();
        } else {
            topFrame = (WsFrame) tFrame;
        }
		

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;
        ImageIcon imgIcon16 = null;


        // Create the help set here.
        try {
            // ???????????????????????
            //URL hsURL = HelpSet.findHelpSet(null, "./help/Template.hs");
            URL hsURL = HelpSet.findHelpSet(null, "com/bluecraft/dbpad/help/Template.hs");

            //System.err.println(hsURL.toString());
            hs = new HelpSet(null, hsURL);
        } catch (Exception exc) {
            System.err.println("HelpSet not found");
            //topFrame.writeError(exc.toString());
            //System.out.println("HelpSet "+helpsetName+" not found");
            //return;
        }

        if(hs != null) {
            hb = hs.createHelpBroker();

        // Help contents
            imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"help.gif"));
            imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"help16.gif"));
            helpContentsAction = new AbstractAction("DBPad Contents", imgIcon) {
                    public void actionPerformed(ActionEvent e) {
                        // Consult JavaHelp documentation!
                        new CSH.DisplayHelpFromSource(hb).actionPerformed(e);
                    }
                };
            button = toolBar.add(helpContentsAction);
            if(bToolBarWithText) {
                button.setText("Help");
            } else {
                button.setText(""); //an icon-only button
            }
            button.setToolTipText("Help Topics");
            button.setMargin(TOOLBARBUTTON_INSETS);
            menuItem = add(helpContentsAction);
            menuItem.setIcon(imgIcon16);
            menuItem.setMnemonic((int)'C');

            //
            //addSeparator();
        }        


        // About_Box Dialog
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"about.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"about16.gif"));
        aboutAction = new AbstractAction("About DBPad...", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    helpAbout_actionPerformed(e);
                }
            };
        button = toolBar.add(aboutAction);
        if(bToolBarWithText) {
            button.setText("About");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("About DBPad");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(aboutAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'A');


        // Add space between toolbar groups
        //toolBar.addSeparator();
    }



    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Action event detected."
            + "    Event source: " + source.getText();
    }
    
    // Help | About
    public void helpAbout_actionPerformed(ActionEvent e) {
        Thread aboutWin = new Thread(new AboutWindow(topFrame));
        aboutWin.run();
    }
    
    
    private boolean openBrowser(String strURL) {
        final String WIN_CMD = "rundll32";
        final String WIN_FLAG = "url.dll,FileProtocolHandler";
    
        String osName = System.getProperty("os.name");
        if ( osName != null && osName.startsWith("Windows")) {
            try {
                String cmd = WIN_CMD + " " + WIN_FLAG + " " + strURL;
                Process p = Runtime.getRuntime().exec(cmd);
            }
            catch(IOException exc)
                {
                    System.err.println("Could not open browser");
                    exc.printStackTrace();
                    return false;
                }
            return true;
        } else {
            System.err.println("Only Windows is supported currently.");
            return false;
        }
    }   
}





