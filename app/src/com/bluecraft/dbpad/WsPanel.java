/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WsPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;


/**
WsPanel is the main panel of WorkSheet class.
It is contained in WsFrame, which contains worksheet menus and toolbars.
WsPanel contains two main panels of WorkSheet: QueryPanel, and ResultPanel.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class WsPanel extends JPanel implements StaticStringList {

    // Once query is excueted, it is stored in Query List.
    public java.util.List   queryList = null;
    public ListIterator  listIterator;
    private  boolean bForward;


    // Internal panels
    JSplitPane splitPane = null;
    JPanel topPane = null;
    JPanel bottomPane = null;
    JTextPane queryPane = null;
    JPanel resultPane = null;

    // Store the main frame as internal variable
    private WsFrame topFrame = null;

    public WsPanel() {
        this(null);
    }
    public WsPanel(JFrame frame) {
        super();
        topFrame = (WsFrame) frame;

        queryList = new ArrayList();
        queryList.add(queryList.size(), new String(""));
        listIterator = queryList.listIterator();
        bForward = false;  // ListIterator is really funny!!!!!!!! Mixing backward and forward moves is so confusing!!!!!

        
        //setBackground(Color.white);
        setLayout(new BorderLayout());
        
        splitPane = new JSplitPane();
        
        splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitPane.setContinuousLayout(true);
        splitPane.setOneTouchExpandable(true);
        //splitPane.setLastDividerLocation(300);
        //splitPane.setBounds(0,0,600,420);
        //splitPane.setPreferredSize(new Dimension (600,420));
       
        
        // Add top panels
        queryPane = new QueryPanel(topFrame);
        topPane = new JPanel();
        topPane.setLayout(new BorderLayout());
        topPane.add(queryPane, BorderLayout.CENTER);

        //resultPane = new ResultPanel();
        resultPane = new JPanel();
        bottomPane = new JPanel();
        bottomPane.setLayout(new BorderLayout());
        bottomPane.add(resultPane, BorderLayout.CENTER);
        
        splitPane.setTopComponent(topPane);
        splitPane.setBottomComponent(bottomPane) ;
        //splitPane.setNextFocusableComponent(topPane);  // ???
        
        
        // Add splitter panel here. This splitter is the only panel inside WsPanel.
        add(splitPane, BorderLayout.CENTER);
    }

    public void showResultPane() {
        bottomPane.setVisible(true);
    }
    
    public void hideResultPane() {
        bottomPane.setVisible(false);
    }
    
    public void parseQuery() {

        String strBuffer = queryPane.getText();
        StringTokenizer st = new StringTokenizer(strBuffer, ";");
        while(st.hasMoreTokens()) {
            String strQ = st.nextToken();
            String strQuery = strQ.trim();
        
            if(strQuery != null && strQuery.length() > 0) {

                // [1] Check sript file (@). Do we need to do this????

                // [2] Then check arguments (&)
                
                // Print out the SQL statement
                System.out.println("> " + strQuery +";");

            }
        }
    }



    public void runQuery() {
        // [1] Append it to the buffer
        appendQueryToList();

        // [2] Execute the query
        Connection con = topFrame.connection;
        Statement stmt = null;
        try {
            stmt = con.createStatement();
        } catch(Exception exc) {
            exc.printStackTrace();
        }


        //
        // Should we allow multiple SQL statements (in a buffer) to run in one execution??????
        //
        String strBuffer = queryPane.getText();
        StringTokenizer st = new StringTokenizer(strBuffer, ";");
        while(st.hasMoreTokens()) {
            String strQ = st.nextToken();
            String strQuery = strQ.trim();
        
            if(strQuery != null && strQuery.length() > 0) {

                // [1] Check sript file (@). Do we need to do this????

                // [2] Then check arguments (&)
                
                // Print out the SQL statement
                System.out.println("> " + strQuery +";");

                // Exit if ...
                if(strQuery.startsWith("exit")) {
                    break;
                }

                //
                // SelectQuery and UpdateQuery
                //
                
                // Execute the SQL statement and print out its result    
                try {
                    ResultSet rset = stmt.executeQuery(strQuery);
                    ResultSetMetaData rsmd = rset.getMetaData();
                    int colCount = rsmd.getColumnCount();

                    for(int i=1;i<=colCount;i++) {
                        System.out.print(rsmd.getColumnName(i) + "\t");
                    }
                    System.out.println("");
                    
                    while (rset.next()) {
                        for(int j=1;j<=colCount;j++) {
                            System.out.print(rset.getString(j)+"\t");
                        }
                        System.out.println("");
                    }

                } catch(Exception exc) {
                    exc.printStackTrace();
                }
            }
        }

        try {
            stmt.close();
        } catch(Exception exc) {
            exc.printStackTrace();
        }
    }

    
    public void appendQueryToList() {
        if(bForward) {
            if(!queryPane.getText().equals((String) queryList.get(listIterator.previousIndex()))) {
                //queryList.set(listIterator.previousIndex(), queryPane.getText());
                queryList.add(queryList.size(), queryPane.getText());
                listIterator = queryList.listIterator(queryList.size());

                topFrame.wsHistoryMenu.upAction.setEnabled(true);
                topFrame.wsHistoryMenu.downAction.setEnabled(false);
            }
        } else {
            if(!queryPane.getText().equals((String) queryList.get(listIterator.nextIndex()))) {
                //queryList.set(listIterator.nextIndex(), queryPane.getText());
                queryList.add(queryList.size(), queryPane.getText());
                listIterator = queryList.listIterator(queryList.size()-1);

                topFrame.wsHistoryMenu.upAction.setEnabled(true);
                topFrame.wsHistoryMenu.downAction.setEnabled(false);
            }
        }
    }

    public void newQuery() {
        if(queryPane.getText().trim().length() != 0) {
            if(bForward) {
                queryList.set(listIterator.previousIndex(), queryPane.getText());
                queryList.add(queryList.size(), new String(""));
                listIterator = queryList.listIterator(queryList.size());
            } else {
                queryList.set(listIterator.nextIndex(), queryPane.getText());
                queryList.add(queryList.size(), new String(""));
                listIterator = queryList.listIterator(queryList.size()-1);
            }
            queryPane.setText("");
            topFrame.wsHistoryMenu.upAction.setEnabled(true);
            topFrame.wsHistoryMenu.downAction.setEnabled(false);
        }
    }

    public void getPreviousQuery() {
        if(bForward) {
            if(listIterator.hasPrevious()) {
                listIterator.previous();
            }
        }
        
        if(listIterator.hasPrevious()) {
            queryList.set(listIterator.previousIndex()+1, queryPane.getText());
            queryPane.setText((String) listIterator.previous());
            topFrame.wsHistoryMenu.downAction.setEnabled(true);
        } else {
            //topFrame.wsHistoryMenu.upAction.setEnabled(false);
        }
        if(!listIterator.hasPrevious()) {
            topFrame.wsHistoryMenu.upAction.setEnabled(false);
        }

        bForward = false;
    }
    
    public void getNextQuery() {
        if(!bForward) {
            if(listIterator.hasNext()) {
                listIterator.next();
            }
        }
        
        if(listIterator.hasNext()) {
            queryList.set(listIterator.nextIndex()-1, queryPane.getText());
            queryPane.setText((String) listIterator.next());
            topFrame.wsHistoryMenu.upAction.setEnabled(true);
        } else {
            //topFrame.wsHistoryMenu.downAction.setEnabled(false);
        }
        if(!listIterator.hasNext()) {
            topFrame.wsHistoryMenu.downAction.setEnabled(false);
        }

        bForward = true;
    }
    

    // For test purposes only
    public static void main(String[] args) {
        LookFeel.initializeLF();
        
        JFrame frame = new JFrame();
        frame.setContentPane(new WsPanel(frame));
        frame.setTitle("WsPanel Demo");
        frame.setVisible(true);
    }
}




