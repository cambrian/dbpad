/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ProjectTree.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;


/**
ProjectTree displays the project file of DBPad in Java.
This class is currently empty. Need to add functionalities for project file access and display.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ProjectTree extends JTree implements StaticStringList {
    
    ResourceBundle RB = ResourceBundle.getBundle("com.bluecraft.dbpad.resource.MainResBundle");
        
    protected static TreeModel getDefaultTreeModel() {
        
        // Add the root node, which is never visible!
        ProjectNode rootNode = new ProjectNode();
        
        // Temporary!!!
        ProjectNode dateNode = new ProjectNode((new Date()).toString(), NODETYPE_FAILSUCC);
        ProjectNode commentNode = new ProjectNode("This is a comment...", NODETYPE_COMMENT);
        ProjectNode warningNode = new ProjectNode("This is a warning...", NODETYPE_WARNING);
        ProjectNode failureNode = new ProjectNode("Operation failed", NODETYPE_FAILURE);
        ProjectNode successNode = new ProjectNode("Operation succeeded", NODETYPE_SUCCESS);
        ProjectNode unknownNode = new ProjectNode("Unknown error occurred...", NODETYPE_UNKNOWN);
        ProjectNode hourglassNode = new ProjectNode("Hour glass...", NODETYPE_HOURGLASS);
        rootNode.add(dateNode);
        dateNode.add(commentNode);
        dateNode.add(warningNode);
        dateNode.add(failureNode);
        dateNode.add(successNode);
        dateNode.add(unknownNode);
        dateNode.add(hourglassNode);
        // Temporary!!!
        
        
        return new DefaultTreeModel(rootNode);
    }

    public ProjectTree() {
                        
        // Set tree model
        super(getDefaultTreeModel());
        
        // Some rendering preferences
		setEditable(false);
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        setShowsRootHandles(true);
		setRootVisible(false);
                
                
        // Set icon images
        // ProjectTreeCellRenderer is derived from DefaultTreeCellRenderer
        // only for this purpose.
        DefaultTreeCellRenderer cellRenderer = new ProjectTreeCellRenderer();
        setCellRenderer(cellRenderer);
        
        
        //Listen for when the selection changes. 
        addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                ProjectNode currentNode = (ProjectNode) (e.getPath().getLastPathComponent());
                int nodetype = currentNode.getNodeType();
                // temporary
                System.out.println ("Node: " + currentNode.toString() + " -> Project Type: " + nodetype);
            } 
        });  
    }
    
    

    // TODO: Implement necessary interfaces

    // Get project texts
    // public String getProjectString(...)...

    // Add/append project entries...
    // public void addProjectEntry(...)...
    
    
}




