/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: AboutWindow.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;


/**
AboutWindow is a dialog window for "about DBPad".

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class AboutWindow extends JWindow implements Runnable, StaticStringList {

    final static String DEFAULT_ABOUT_IMAGE = "images"+fileSep+"aboutDBPad.gif";
    // Dismiss it after ABOUT_DURATION even if the user haven't done so.
    final static int    ABOUT_DURATION = 45000;  // in mili seconds
    
    JLabel imgLabel = null;
    Timer  timer = null;
  
    // Main frame window
    private JFrame frame = null;
    
    public AboutWindow() {
        this(null, DEFAULT_ABOUT_IMAGE);
    }
    
    public AboutWindow(JFrame frame) {
        this(frame, DEFAULT_ABOUT_IMAGE);
    }
    
    // This is the most commonly used constructor
    /**
    @param frame Main frame window of the application
    @param imgName  Image File for the About Dialog
    */
    public AboutWindow(JFrame frame, String imgName) {
        super(frame);
        this.frame = frame;
        
        imgLabel = new JLabel(new ImageIcon(MainFrame.class.getResource(imgName)));
        imgLabel.setBorder(BorderFactory.createRaisedBevelBorder());
        //imgLabel.setPreferredSize(new Dimension(500, 400));

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(imgLabel, BorderLayout.CENTER);
        
        // ABOUT screen stays for ABOUT_DURATION (mSec) by default
        timer = new Timer(ABOUT_DURATION, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Timer timer = (Timer) e.getSource();
                if(AboutWindow.this.timer == timer) {
                    AboutWindow.this.closeAboutWindow();
                }
            }
        });
        
        // When the user clicks this about dialog,
        // it should disappear.
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                AboutWindow.this.closeAboutWindow();
            }
        });

    }
    
    public void showAboutWindow() {
        centerAboutWindow();
        setVisible(true);
    }
    
    // Center the about-dialog window relative to the main frame window
    private void centerAboutWindow() {
        Dimension imgSize = imgLabel.getPreferredSize();
        
        if(frame != null) {
            Point frameLocation = frame.getLocation();
            Dimension frameSize = frame.getSize();
            setLocation((frameLocation.x + frameSize.width/2) - (imgSize.width/2),
                        (frameLocation.y + frameSize.height/2) - (imgSize.height/2));
        } else {
            Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((scrnSize.width/2) - (imgSize.width/2),
                        (scrnSize.height/2) - (imgSize.height/2));
        }
        pack();
    }
    
    private void closeAboutWindow() {
        timer.stop();
        dispose();
        //System.exit(0);  // temporary....
    }

    // AboutWindow uses a separate thread from the main application.
    public void run() {
        showAboutWindow();
        timer.start();
    }
    

    // Copy the following two lines into your program.
    // Normally, before you start your main frame window...
    public static void main(String[] args) {
        Thread aWin = new Thread(new AboutWindow());
        aWin.run();
    }   

}




