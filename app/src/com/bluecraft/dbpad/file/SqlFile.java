/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: SqlFile.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad.file;


import java.io.*;
     
/**
SqlFile extends File.
SqlFile contains SQL statements.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class SqlFile extends File {
    
    public boolean  bModified = true;
    

    /**
    @param parent Pathname in which the file is created.
    @param name Name of the file to be created.
    */
    public SqlFile(String parent, String name) {
        super(parent, name);
    }
    /**
    @param parent File object in which the file is created.
    @param name Name of the file to be created.
    */
    public SqlFile(File parent, String name) {
        super(parent, name);
    }
    /**
    @param pathname Full pathname of the file to be created.
    */
    public SqlFile(String pathname) {
        super(pathname);
    }



    ////////////////////////////////
    public String toString() {
        // temporary
        return getPath();
    }
    


    /**
    This function is provided for testing purposes.
    */
    public static void main(String[] args) {
        SqlFile rFile = new SqlFile(args[0]);
        //rFile.doSomething();
    }
}




