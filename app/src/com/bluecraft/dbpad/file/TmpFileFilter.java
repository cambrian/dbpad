/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: TmpFileFilter.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad.file;


import java.io.*;
import javax.swing.*;
     
     
/**
TmpFileFilter extends javax.swing.filechooser.FileFilter.
It accepts Tmp, which is just a generic file (or temporary file).
Accepted extensions: .tmp

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class TmpFileFilter extends javax.swing.filechooser.FileFilter {

    public TmpFileFilter() {
    }

    public boolean accept(File pathName) {
        if(pathName.isDirectory()) {
            return true;
        } else if(pathName.isFile() && 
            (pathName.getName().endsWith(".tmp")
        )) {
            return true;
        } else {
            return false;
        }
    }
    
    public String getDescription() {
        return "Temporary Files (*.tmp)";
    }
}



