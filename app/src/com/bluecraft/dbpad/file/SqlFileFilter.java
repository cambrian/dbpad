/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: SqlFileFilter.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad.file;


import java.io.*;
import javax.swing.*;
     
     
/**
SqlFileFilter extends javax.swing.filechooser.FileFilter.
It accepts Sql script file.
Accepted extensions: .sql

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class SqlFileFilter extends javax.swing.filechooser.FileFilter {

    public SqlFileFilter() {
    }

    public boolean accept(File pathName) {
        if(pathName.isDirectory()) {
            return true;
        } else if(pathName.isFile() && 
            (pathName.getName().endsWith(".sql")
        )) {
            return true;
        } else {
            return false;
        }
    }
    
    public String getDescription() {
        return "SQL Files (*.sql)";
    }
}



