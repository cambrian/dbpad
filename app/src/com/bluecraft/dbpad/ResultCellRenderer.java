/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ResultCellRenderer.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;


/**
ResultCellRenderer
                
@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ResultCellRenderer extends DefaultTableCellRenderer implements StaticStringList {
    
    private static final ImageIcon upIcon = new ImageIcon(ResultCellRenderer.class.getResource("images"+fileSep+"resultupicon.gif"));
    private static final ImageIcon downIcon = new ImageIcon(ResultCellRenderer.class.getResource("images"+fileSep+"resultdownicon.gif"));

    private Border selBorder = null;
    private Font font = null;

    public ResultCellRenderer() { 
	    super();
	    
	    ///
	    setForeground(Color.black);
	    setBackground(Color.white);
	    
        //Border loweredBorder = new BevelBorder(BevelBorder.LOWERED, Color.white, Color.black);
        Border loweredBorder = new BevelBorder(BevelBorder.LOWERED);
        selBorder = new CompoundBorder(loweredBorder, new EmptyBorder(3,5,3,5));
        
        // temporary!!!
        //font = getFont();
        // font = new Font(...);
    }
   
    public Component getTableCellRendererComponent(JTable table,
                                             Object value,
                                             boolean isSelected,
                                             boolean hasFocus,
                                             int row,
                                             int column) {
        JLabel resultLabel = 
            (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        //temporary
        resultLabel.setBorder(new EmptyBorder(3,5,3,5));
        // this is better than table.getColumnModel().setColumnMargin(5)!!!!
        //                and  table.setRowMargin(3);
        
        //
        //resultLabel.setFont(font);
        
        
        /*
        resultLabel.addFocusListener(new FocusAdapter() {
            public void focusGained(FocusEvent e) {
	            System.out.println("Focus gained: " + e.toString());
            }
            public void focusLost(FocusEvent e) {
	            System.out.println("Focus lost: " + e.toString());
            }
        });
        */
        
        // column==0 means the cell value is instanceof ResultClass
        // column information is *hard-coded*
        /*
        if(column == 0) { 
            if(value != null && value instanceof ResultClass) {
                ResultClass ifClass = (ResultClass) value;
                if(ifClass.isUp) {
                    resultLabel.setIcon(upIcon);
                } else {
                    resultLabel.setIcon(downIcon);
                }
            } else {
                resultLabel.setIcon(downIcon);
            }
        }
        */
        //temporary
        
        if(table != null && table instanceof ResultTable)  {
            ResultTable ifTable = (ResultTable) table;
            if(row == ifTable.selRow && column == ifTable.selColumn) {
                resultLabel.setBorder(selBorder);
                //resultLabel.setForeground(Color.red);
            }
        }
        
        // Set tooltip text for the table header...
        // Temporary.
        /*  //????????????????????????
        if(value.equals("Interface")) {
            resultLabel.setToolTipText("Interface");
        }
        */
        
        
        //////////////////
        return resultLabel;
    }
    
}



