/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ViewMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


/**
ViewMenu is a default view menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ViewMenu extends JMenu implements AppConstantList, StaticStringList {
        
    boolean bToolBarWithText = false;
    boolean bShowStatusBar = true;
    boolean bShowToolBar = true;
    boolean bShowCommandPane = true;
    boolean bShowLogPane = true;

    // Store the main frame as internal variable
    private MainFrame topFrame = null;


    ViewMenu(JToolBar toolBar) {
        this(null, toolBar);
    }

    ViewMenu(JFrame tFrame, JToolBar toolBar) {

        super("View");
		setMnemonic((int)'V');

		// Set the main frame
		if(tFrame == null) {
		    topFrame = (MainFrame) getTopLevelAncestor();
		} else {
            topFrame = (MainFrame) tFrame;
        }

		if(topFrame != null) {
		    bToolBarWithText = topFrame.bToolBarWithText;
		    bShowStatusBar = topFrame.bShowStatusBar;
		    bShowToolBar = topFrame.bShowToolBar;
		    bShowCommandPane = topFrame.bShowCommandPane;
		    bShowLogPane = topFrame.bShowLogPane;
		}
	
        JCheckBoxMenuItem cbMenuItem = null;

	    // Toggle ToolBar
        cbMenuItem = new JCheckBoxMenuItem("Toolbar");
        cbMenuItem.setSelected(bShowToolBar);
        //cbMenuItem.setEnabled(true);
        cbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JCheckBoxMenuItem mi = (JCheckBoxMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    bShowToolBar = true;
                    topFrame.toolBar.setVisible(true);
                } else {
                    bShowToolBar = false;
                    topFrame.toolBar.setVisible(false);
                }
                topFrame.invalidate();
                topFrame.validate();
                //topFrame.repaint();
                topFrame.bShowToolBar = bShowToolBar;
            }
        });
		cbMenuItem.setMnemonic((int)'T');
        add(cbMenuItem);

	    // Toggle Command Window
        cbMenuItem = new JCheckBoxMenuItem("Command Window");
        cbMenuItem.setSelected(bShowCommandPane);
        //cbMenuItem.setEnabled(true);
        cbMenuItem.addItemListener(new ItemListener() {
            // Temporary!
            // This should be implemented with *two appPanes*, one with splitter and one without splitter.
            // Then, this command simply changes topFram.appPane between appPane_split and appPane_nosplit
            // Also, check out if Swing has a simple function for this as in MFC!
            public void itemStateChanged(ItemEvent e) {
                JCheckBoxMenuItem mi = (JCheckBoxMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    bShowCommandPane = true;
                    topFrame.appPane.splitPane.setBottomComponent(topFrame.appPane.bottomPane);
                    topFrame.appPane.splitPane.resetToPreferredSizes();
                } else {
                    bShowCommandPane = false;
                    topFrame.appPane.splitPane.remove(topFrame.appPane.bottomPane);
                }
                topFrame.invalidate();
                topFrame.validate();
                //topFrame.repaint();
                topFrame.bShowCommandPane = bShowCommandPane;
            }
        });
		cbMenuItem.setMnemonic((int)'C');
        add(cbMenuItem);


    	// Toggle StatusBar
        cbMenuItem = new JCheckBoxMenuItem("Status Bar");
        cbMenuItem.setSelected(bShowStatusBar);
        //cbMenuItem.setEnabled(true);
        cbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JCheckBoxMenuItem mi = (JCheckBoxMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    bShowStatusBar = true;
                    topFrame.statusBar.setVisible(true);
                } else {
                    bShowStatusBar = false;
                    topFrame.statusBar.setVisible(false);
                }
                topFrame.invalidate();
                topFrame.validate();
                //topFrame.repaint();
                topFrame.bShowStatusBar = bShowStatusBar;
            }
        });
		cbMenuItem.setMnemonic((int)'A');
        add(cbMenuItem);

        addSeparator();

	    JMenu lookFeelMenu = createLookFeelMenu();
	    add(lookFeelMenu);
        

        // Add space between toolbar groups
        //toolBar.addSeparator();
    }


    // LookandFeel Menu
    protected JMenu createLookFeelMenu() {
        JMenu lfMenu = new JMenu("Look/Feel");
        
        //a group of radio button menu items
        ButtonGroup group = new ButtonGroup();
        JRadioButtonMenuItem rbMenuItem = null;
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.METAL_LF_NAME);
        rbMenuItem.setSelected(true);  // Meta LF is the default
        rbMenuItem.setEnabled(LookFeel.isMetalLnfSupported());
        group.add(rbMenuItem);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    LookFeel.changeLF(topFrame, LookFeel.METAL_LF);
                }    
            }
        });
        lfMenu.add(rbMenuItem);
        
        lfMenu.addSeparator();
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.WINDOWS_LF_NAME);
        rbMenuItem.setSelected(false);
        rbMenuItem.setEnabled(LookFeel.isWindowsLnfSupported());
        group.add(rbMenuItem);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    LookFeel.changeLF(topFrame, LookFeel.WINDOWS_LF);
                }    
            }
        });
        lfMenu.add(rbMenuItem);
        
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.MOTIF_LF_NAME);
        rbMenuItem.setSelected(false);
        rbMenuItem.setEnabled(LookFeel.isMotifLnfSupported());
        group.add(rbMenuItem);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    LookFeel.changeLF(topFrame, LookFeel.MOTIF_LF);
                }    
            }
        });
        lfMenu.add(rbMenuItem);
        
        // Mac Look and Feel
        rbMenuItem = new JRadioButtonMenuItem(LookFeel.MAC_LF_NAME);
        rbMenuItem.setSelected(false);
        rbMenuItem.setEnabled(LookFeel.isMacLnfSupported());
        group.add(rbMenuItem);
        rbMenuItem.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                JRadioButtonMenuItem mi = (JRadioButtonMenuItem)(e.getSource());
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                if(selected) {
                    LookFeel.changeLF(topFrame, LookFeel.MAC_LF);
                }    
            }
        });
        lfMenu.add(rbMenuItem);

        return lfMenu;
    }

}





