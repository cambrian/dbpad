/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ResultCellEditor.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;



/**
ResultCellEditor
                
@author   H. Yoon
@version  $Revision: 1.2 $
*/
// TODO:
// implement AbstractCellEditor interface rather than derive from DefaultCellEditor
public class ResultCellEditor extends DefaultCellEditor {
    
    public ResultCellEditor() { 
	    super(new JTextField());
    }
    
    public ResultCellEditor(JTextField tf) { 
	    super(tf);
	    
	    // ????
	    //clickCountToStart = 0;
    }

}



