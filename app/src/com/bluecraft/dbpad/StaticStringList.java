/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: StaticStringList.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


/**
StaticStringList lists all static string-constants used in DBPad in Java.
All classes of Application Template should implement this interface.
Note that human-readable strings should be included in resource bundles!

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public interface StaticStringList {

    // String Constants
    
    
    // Enumerators
    static final int NODETYPE_UNKNOWN = 0;
    static final int NODETYPE_COMMENT = 1;
    static final int NODETYPE_WARNING = 2;
    static final int NODETYPE_HOME = 3;
    static final int NODETYPE_SUCCESS = 4;
    static final int NODETYPE_FAILURE = 5;
    static final int NODETYPE_FAILSUCC = 6;
    static final int NODETYPE_HOURGLASS = 7;
    
    
    // Miscellaneous
    static final String fileSep = System.getProperty("file.separator");

}    





