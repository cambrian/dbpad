/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ResultTable.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;


/**
ResultTable.
                
@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ResultTable extends JTable implements StaticStringList {
    
    private static int[]    columnWidths = {150, 150, 100, 150, 100};
    private static int[]    columnMinWidths = {75, 75, 50, 75, 50};
        
    private JPanel parentPanel = null;
    
    //private int selRow = -1;
    //private int selColumn = 0;
    public int selRow = -1;
    public int selColumn = 0;
    
    
    /**
    Constructors
    */
    public ResultTable() {
        this(new ResultTableModel());
    }
    public ResultTable(JPanel parentPanel) {
        this(new ResultTableModel(), parentPanel);
    }
    public ResultTable(DefaultTableModel tableModel) {
        this(tableModel, null);
    }
    public ResultTable(DefaultTableModel tableModel, JPanel parentPanel) {
        super(tableModel);
        this.parentPanel = parentPanel;
        
        //<< Change some visual settings of the table
        ////
        rowHeight = 23;
        //rowMargin = 3;
        ////
        selectionForeground = Color.red;
        selectionBackground = Color.yellow;
        ////
        //getColumnModel().setColumnMargin(5);
        //This is taken care of in TableCellRenderer class!!!
        //>> Change some visual settings of the table
        
        
        
        // Enable cell selection
        // Do I also need row and column selections?
        setColumnSelectionAllowed(true);
        setRowSelectionAllowed(true);
        setCellSelectionEnabled(true);
        
        
        // Setting minimum size does *not* work properly with AUTO_RESIZE!!!!!!!!!!!!!!!!
        for(int i=0;i<columnWidths.length;i++) {
            TableColumn tblColumn = getColumnModel().getColumn(i);
            tblColumn.setPreferredWidth(columnWidths[i]);
            //tblColumn.setMinWidth(columnMinWidths[i]); //??????????
        }
        sizeColumnsToFit(-1); //?????
        ///////////////////////////////////////////////////////////
        
        //
        for(int j=0;j<columnWidths.length;j++) {
            TableColumn tColumn = getColumnModel().getColumn(j);
            tColumn.setCellRenderer(new ResultCellRenderer());
        }
        
        //
        for(int k=0;k<columnWidths.length;k++) {
            TableColumn tColumn = getColumnModel().getColumn(k);
            tColumn.setCellEditor(new ResultCellEditor());
        }
        
        // Populate bottom panels
        for(int p=0;p<getRowCount();p++) {
            for(int q=0;q<columnWidths.length;q++) {
                CellClass cell = (CellClass) getValueAt(p, q);
                cell.addPanel(parentPanel);
            }
        }

        
        // ?????
        //setPreferredScrollableViewportSize(new Dimension(200,100));
        
        
        // Set some default behaviors here
        //setAutoResizeMode(JTable.AUTO_RESIZE_OFF);   //?????
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        getTableHeader().setReorderingAllowed(false);
        
        //
            /*
        getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) {
                    selRow = getSelectedRow();
                    System.out.println("Row selection changed: " + selRow);
                } else {
                    // Selection is constantly changing....
                }
            }
        });
            */
            
            
        // TODO: Column sorting?????    
        // or template dialog?????
        // For now, let's just use sorting...
        JTableHeader thdr = (JTableHeader) getTableHeader();
        thdr.setToolTipText("Click here to sort the table");
        thdr.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                
                // Show visual look of a pressed button
                //
                
            }
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                
                // Restore the button's visual look
                //
                
            }
            public void mouseClicked(MouseEvent e) {
                //super.mouseClicked(e);
                
                if(getRowCount() <= 0) {
                    return;
                }
                
                TableColumnModel tcm = getColumnModel();
                int col = convertColumnIndexToModel(tcm.getColumnIndexAtX(e.getX()));
                //System.out.println("Table Width: " + tcm.getTotalColumnWidth());
                //
                // call the sorting function of the *table model*!!!!
                // or bring up a dialog window for "batch mode editing"....
                //
                
                // For now, let's just use sorting...
                ResultTableModel model = (ResultTableModel) getModel();
                selRow = model.sortRow(col, selRow);
                
                //
                //TableColumn tcl = tcm.getColumn(selColumn);
                getSelectionModel().setSelectionInterval(selRow, selRow);
                //revalidate();
            }
        });
        
    }


    // (1) Row selection changed
    public void valueChanged(ListSelectionEvent e) {
        super.valueChanged(e);
        /////////////
        if(!e.getValueIsAdjusting()) {
            selRow = getSelectedRow();
            //System.out.println("Row selection changed: " + selRow);
            // If both row and column selections change, call updateBottomPane() only once!!!!
            if(selColumn == getSelectedColumn()) {
                updateBottomPane();
            }
        }
        /////////////
    }

    // (2) Column selection changed
    public void columnSelectionChanged(ListSelectionEvent e) {
        super.columnSelectionChanged(e);
        //////////////
        if(!e.getValueIsAdjusting()) {
            selColumn = getSelectedColumn();
            //System.out.println("Column selection changed: " + selColumn);
            updateBottomPane();
        } 
        /////////////
    }

    // (3) Change the lower panel.....
    private void updateBottomPane() {
        if(selRow < 0) { // selRow == -1
            showEmptyPane();        
        } else if(selRow >= getRowCount()){
            //??????
        } else {
            //////
            CellClass cell = (CellClass) getModel().getValueAt(selRow, selColumn);
            //System.out.println(cell.toString());
            //////
            if(parentPanel == null) {
                    System.out.println("Parent Panel is null???");
                    return;
            }
            cell.showPanel(parentPanel);
            //////
        }
    }

    private void showEmptyPane() {
        /*
        JPanel card = ((ResultPanel) parentPanel).cardPane;
        CardLayout cl = (CardLayout)(card.getLayout());
        cl.show(card, ((ResultPanel) parentPanel).emptyPane.getUniqueName());
        parentPanel.revalidate();
        parentPanel.repaint();
        */
    }
    
    /////////////////////////////////////////////////
    // Append a new row.
    /*
    public void addInterface(ResultClass ResultRow) {
        ///
        CellClass[] rowData = {ResultRow,
                                ResultRow.wanProtocol,
                                ResultRow.ipAddress,
                                ResultRow.routeFunction,
                                ResultRow.accessList};
        ResultRow.addPanel(parentPanel);
        ResultRow.wanProtocol.addPanel(parentPanel);
        ResultRow.ipAddress.addPanel(parentPanel);
        ResultRow.routeFunction.addPanel(parentPanel);
        ResultRow.accessList.addPanel(parentPanel);
        
        ResultTableModel tableModel = (ResultTableModel) getModel();
        tableModel.addRow(rowData); 
        
        // If the table was empty (selRow == -1), select the first row 
        if(selRow == -1) {
            selRow = 0;
            getSelectionModel().setSelectionInterval(selRow, selRow);
        }
        // TODO: When the table is created empty, this does not work. Why?????
    }
    */
    /*
    public void insertInterface(ResultClass ResultRow, int row) {
        ///
        CellClass[] rowData = {ResultRow,
                                ResultRow.wanProtocol,
                                ResultRow.ipAddress,
                                ResultRow.routeFunction,
                                ResultRow.accessList};
        ResultRow.addPanel(parentPanel);
        ResultRow.wanProtocol.addPanel(parentPanel);
        ResultRow.ipAddress.addPanel(parentPanel);
        ResultRow.routeFunction.addPanel(parentPanel);
        ResultRow.accessList.addPanel(parentPanel);
        
        ResultTableModel tableModel = (ResultTableModel) getModel();
        tableModel.insertRow(row, rowData); 
        /// 
    }
    */
    // Remove the last row.
    public void removeInterface() {
        /*
        int lastRow = getRowCount() - 1;
        if(lastRow < 0) {
            return;
        }
        //
        ResultClass ResultRow = (ResultClass) getValueAt(lastRow,0);
        //
        ResultTableModel tableModel = (ResultTableModel) getModel();
        tableModel.removeRow(lastRow);
        
		// TODO: change selRow if the current row is selected
		if(lastRow == selRow) {
		    selRow = lastRow-1;
            getSelectionModel().setSelectionInterval(selRow, selRow);
            if(selRow >= 0) {
		        // change lower card panel: done.
		    } else { // selRow == -1
                showEmptyPane();        
		    }
		}
		        
		// TODO: remove card panels associated with the ResultClass being deleted!!!!
		//temporary
		ResultRow = null;
		//temporary
        */
    }
    /*
    public void removeInterface(int row) {
        if(row < 0 || row >= getRowCount()) {
            // ????
            return;
        }
        
        ResultTableModel tableModel = (ResultTableModel) getModel();
        tableModel.removeRow(row);
        
		// TODO: change selRow if the current row is selected
		
		        
		// TODO: change lower card panel
		//
		        
		// TODO: remove card panels!!!!
		//
    }
    public void removeInterface(ResultClass ResultRow) {
       ///
        ResultTableModel tableModel = (ResultTableModel) getModel();
		for(int i=0;i<tableModel.getRowCount();i++) {
		    if(ResultRow == (ResultClass) getValueAt(i,0)) {
		        tableModel.removeRow(i);
		        
		        // TODO: change selRow if the current row is selected
		        //
		        
		        // TODO: change lower card panel
		        //
		        
		        // TODO: remove card panels!!!!
		        //
		        
		        break;
		    }
		}
       /// 
    }
    */
    public void cleanUp() {
        //
        ResultTableModel tableModel = (ResultTableModel) getModel();
		for(int i=tableModel.getRowCount();i>0;i--) {
		    removeInterface();
		}
		//
    }
    
    /*
    public void displayInterfaces(SlotListTable slotTable) {
        // Clean up the table
        cleanUp();
        repaint();
        
		// Copy rows from slotTable
		int  slotIdx = -1;  // First slot index is 0.
		for(int i=0;i<slotTable.getRowCount();i++) {
            Vector slotRow = (Vector) ((SlotListTableModel) slotTable.getModel()).getDataVector().elementAt(i);
            // Parse
            StringTokenizer tokenizer = null;
            String     strResults = null;
            ResultClass ResultRow = null;
            Object slt = slotRow.elementAt(0);
            if(slt instanceof RmSlotClass) {
                // Increase the slot count
                slotIdx++;
                // Parse
                strResults = slotRow.elementAt(1).toString();
                tokenizer = new StringTokenizer(strResults, ",");
                while (tokenizer.hasMoreTokens()) {
                    String strPair = tokenizer.nextToken();
                    strPair = strPair.trim();
                    //System.out.println(strPair);
                    
                    int spc = strPair.indexOf(" ");
                    int count = Integer.parseInt(strPair.substring(0,spc));
                    String ResultType = strPair.substring(spc+1);
                    if(ResultType.startsWith("WAN") || ResultType.startsWith("VIC")) {
                        continue;
                    }
                    for(int p=0;p<count;p++) {
                        // Add the new interface
                        ResultRow = new ResultClass(ResultType + " " + slotIdx + "/" + p);
                        // temporary
                        //ResultRow.wanProtocol = new  WanProtocol("WanProtocol");
                        //ResultRow.ipAddress = new  IpAddress("IpAddress");
                        //ResultRow.routeFunction = new  RouteFunction("RouteFunction");
                        //ResultRow.accessList = new  AccessList("AccessList");
                        // temporary
                        addInterface(ResultRow);
                    }
                }
            } else if(slt instanceof String) {
                // We do NOT increase the slot count in this case
                
                // Skip empty wan/vic slots
                strResults = (String) slotRow.elementAt(1);
                if(strResults.startsWith("Empty")) {
                    continue;
                }
                
                // parse
                tokenizer = new StringTokenizer(strResults, ",");
                while (tokenizer.hasMoreTokens()) {
                    String strPair = tokenizer.nextToken();
                    strPair = strPair.trim();
                    
                    int spc = strPair.indexOf(" ");
                    int count = Integer.parseInt(strPair.substring(0,spc));
                    String ResultType = strPair.substring(spc+1);
                    for(int p=0;p<count;p++) {
                        // Add the new interface
                        ResultRow = new ResultClass(ResultType + " " + slotIdx + "/" + p);
                        // temporary
                        //ResultRow.wanProtocol = new  WanProtocol("WanProtocol");
                        //ResultRow.ipAddress = new  IpAddress("IpAddress");
                        //ResultRow.routeFunction = new  RouteFunction("RouteFunction");
                        //ResultRow.accessList = new  AccessList("AccessList");
                        // temporary
                        addInterface(ResultRow);
                    }
                }
            } else {
                // ???
            }
        }
    }
    */
    
    /*
    public void displayVirtualInterfaces(ResultClass[] ResultRows) {
        // [1] Remove previous VI's
        //
        
        // [2] Add VI's
        //
        
    }
    */
    /////////////////////////////////////////////////
    





    
    

    /////////////////////////////////////////////////
    public static void main(String[] args) {
        JPanel mainPane = new JPanel();
        mainPane.setLayout(new BorderLayout());
        
        ResultTableModel ResultModel = new ResultTableModel();
		ResultTable ResultTable = new ResultTable(ResultModel, null);
		JScrollPane sPane = new JScrollPane(ResultTable);
        mainPane.add(sPane, BorderLayout.CENTER);
        
        /*
        JButton button = new JButton("test");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ResultClass Result = new ResultClass("ResultClass");
                Result.wanProtocol = new  WanProtocol("WanProtocol");
                Result.ipAddress = new  IpAddress("IpAddress");
                Result.routeFunction = new  RouteFunction("RouteFunction");
                Result.accessList = new  AccessList("AccessList");
                ResultTable.addInterface(Result);
            }
        });
        mainPane.add(button, BorderLayout.NORTH);
        */
        
        JFrame frame = new JFrame();
        frame.setContentPane(mainPane);
        frame.pack();
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);            // close the application
            }
        });
        frame.setVisible(true);
    }

}




