/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: AppPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;


/**
AppPanel is the main panel of DBPad in Java.
It is contained in MainFrame, which contains menus and also toolbars and status bars.
AppPanel contains three main panels of the application: ProjectPanel, SummaryPanel, and CommandPanel.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class AppPanel extends JPanel implements StaticStringList {


    JSplitPane splitPane = null;
    JSplitPane topPane = null;
    JPanel bottomPane = null;
    JPanel projectPane = null;
    JPanel summaryPane = null;
    JTextPane commandPane = null;

    // Store the main frame as internal variable
    private JFrame topFrame = null;

    public AppPanel() {
        this(null);
    }
    public AppPanel(JFrame frame) {
        super();
        //topFrame = (MainFrame) frame;
        topFrame =  frame;
        
        //setBackground(Color.white);
        setLayout(new BorderLayout());
        
        splitPane = new JSplitPane();
        
        splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitPane.setContinuousLayout(true);
        splitPane.setOneTouchExpandable(true);
        //splitPane.setLastDividerLocation(300);
        //splitPane.setBounds(0,0,600,420);
        //splitPane.setPreferredSize(new Dimension (600,420));
       
        
        // Add top panels
        topPane = new JSplitPane();
        topPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        topPane.setContinuousLayout(true);
        topPane.setOneTouchExpandable(true);
        topPane.setPreferredSize(new Dimension (500,300));  // Just an arbitrary number...
        
        projectPane = new ProjectPanel(topFrame);
        summaryPane = new SummaryPanel(topFrame);
        
        topPane.setLeftComponent(projectPane);
        topPane.setRightComponent(summaryPane) ;
        //topPane.setNextFocusableComponent(projectPane);  // ???


        commandPane = new CommandPanel(topFrame);
        
        bottomPane = new JPanel();
        bottomPane.setLayout(new BorderLayout());
        bottomPane.add(commandPane, BorderLayout.CENTER);

        
        splitPane.setTopComponent(topPane);
        splitPane.setBottomComponent(bottomPane) ;
        //splitPane.setNextFocusableComponent(topPane);  // ???
        
        
        // Add splitter panel here. This splitter is the only panel inside AppPanel.
        add(splitPane, BorderLayout.CENTER);
    }

    public void showCommandPane() {
        bottomPane.setVisible(true);
    }
    
    public void hideCommandPane() {
        bottomPane.setVisible(false);
    }
    

    // For test purposes only
    public static void main(String[] args) {
        LookFeel.initializeLF();
        
        JFrame frame = new JFrame();
        frame.setContentPane(new AppPanel(frame));
        frame.setTitle("AppPanel Demo");
        frame.setVisible(true);
    }
}




