/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ConnectDialog.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;


/**
ConnectDialog is derived from InputDialog, 
which is a superclass of (almost) all dialog windows which accept users' inputs.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ConnectDialog extends InputDialog {

    public JLabel       dbLabel  = new JLabel("Database");
    public JTextField   dbTextField = new JTextField(15);
    public JLabel       usernameLabel  = new JLabel("Username");
    public JTextField   usernameTextField = new JTextField(15);
    public JLabel       passwordLabel  = new JLabel("Password");
    public JTextField   passwordTextField = new JTextField(15);

    
    public ConnectDialog() {
        this(null);
    }
    
    public ConnectDialog(JFrame frame) {
        super(frame); 

        // InputDialog is non-resizable. Set ConnectDialog's size here
        setSize(new Dimension(310,170));


        // Add content
        //mainPane.setBackground(BG_COLOR);
        GridBagLayout        tGbl = new GridBagLayout();
        GridBagConstraints   tGbc = new GridBagConstraints();
        mainPane.setLayout(tGbl);
		
        tGbc.anchor = GridBagConstraints.NORTHWEST;
        tGbc.gridwidth = 1;
        mainPane.add(dbLabel, tGbc);
        mainPane.add(Box.createHorizontalStrut(25), tGbc);
        tGbc.gridwidth = GridBagConstraints.REMAINDER;
        mainPane.add(dbTextField, tGbc);
        mainPane.add(Box.createVerticalStrut(8), tGbc);
		
        tGbc.gridwidth = 1;
        mainPane.add(usernameLabel, tGbc);
        mainPane.add(Box.createHorizontalStrut(25), tGbc);
        tGbc.gridwidth = GridBagConstraints.REMAINDER;
        mainPane.add(usernameTextField, tGbc);
        mainPane.add(Box.createVerticalStrut(8), tGbc);
		
        tGbc.gridwidth = 1;
        mainPane.add(passwordLabel, tGbc);
        mainPane.add(Box.createHorizontalStrut(25), tGbc);
        tGbc.gridwidth = GridBagConstraints.REMAINDER;
        mainPane.add(passwordTextField, tGbc);
        mainPane.add(Box.createVerticalStrut(8), tGbc);
		

        
        // Button action listners
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Do something...
                
                // hide the dialog
                hideDialogWindow();
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // hide the dialog (do not dismiss it)
                hideDialogWindow();
            }
        });
        helpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Bring up help window (context-sensitive)
                
            }
        });
        
    }

    

    ////////////////////////////////////////
    public static void main(String[] args) {
        ConnectDialog dialog = new ConnectDialog();
        dialog.showDialogWindow();
    }
    ////////////////////////////////////////

}




