/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: WsFileMenu.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import com.bluecraft.dbpad.file.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.text.*;
import java.io.*;
import java.util.*;


/**
WsFileMenu is a default file menu.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class WsFileMenu extends JMenu implements AppConstantList, StaticStringList {
        
    ResourceBundle RB = ResourceBundle.getBundle("com.bluecraft.dbpad.resource.MainResBundle");

    boolean bToolBarWithText = false;
    
    Action newQueryAction;
    Action openQueryAction;
    Action saveQueryAction;
    Action printAction;
    Action exitAction;
    
    private JFileChooser fileChooser = null;


    //WsFrame topFrame = null;
    WsFrame topFrame = (WsFrame) getTopLevelAncestor();

    WsFileMenu(JFrame topFrame, JToolBar toolBar) {
        this(toolBar);
        this.topFrame = (WsFrame) topFrame;
    }

    WsFileMenu(JToolBar toolBar) {
        
        super("File");
        setMnemonic((int)'F');
		
        // Create file chooser
        fileChooser = new JFileChooser();
        // Set the default directory
        // TODO: Save and load the directory the user used last time.
        File defaultDir = new File(System.getProperty("user.dir"));
        fileChooser.setCurrentDirectory(defaultDir);
        // Set file filters here
        fileChooser.setFileFilter(new SqlFileFilter());
        //fileChooser.addChoosableFileFilter(new SqlFileFilter());
		

        JButton button = null;
        JMenuItem menuItem = null;
        ImageIcon imgIcon = null;
        ImageIcon imgIcon16 = null;

        // New file
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"NewQuery.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"NewQuery16.gif"));
        newQueryAction = new AbstractAction("New", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    //
                    // (1) Clean Query Pane
                    //boolean bResult = cleanUp();
                    //
                    if(topFrame != null) {
                        WsPanel wsPane = (WsPanel) topFrame.wsPane;
                        wsPane.newQuery();
                    }
                    //
                }
            };
        button = toolBar.add(newQueryAction);
        if(bToolBarWithText) {
            button.setText("New");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("New Query");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(newQueryAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'N');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Event.CTRL_MASK));

        // Open Query
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"OpenQuery.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"OpenQuery16.gif"));
        openQueryAction = new AbstractAction("Open...", imgIcon) {
            
                public void actionPerformed(ActionEvent e) {

                    // (0) Ask whether to close the existing Query (if there's one)
                    // (1) Save and close the old sql file
                    boolean bResult = cleanUp();
                
                    if(bResult) {
                        //  Ask for a new file name to be open
                        fileChooser.setDialogTitle("Open File");
                        int state = fileChooser.showOpenDialog(topFrame);
                        File file = fileChooser.getSelectedFile();
                    
                        if(file != null && state == JFileChooser.APPROVE_OPTION) {
                            SqlFile sqlFile = new SqlFile(file.getPath());

                            // ????????????????
                            boolean bSuccessful = readFromFile(sqlFile);
                            if(bSuccessful) {
                                // operation successful
                                //topFrame.writeLog("Opened an sql file " + sqlFile.toString());
                            } else {
                                // something's wrong....
                                //topFrame.writeLog("Failed to open an sql file " + sqlFile.toString());
                            }
                        } else {
                            // operation canceled.
                        }
                    } else {
                        // Didn't close the old file
                    }
                }
            };
        button = toolBar.add(openQueryAction);
        if(bToolBarWithText) {
            button.setText("Open");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Open Query");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(openQueryAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'O');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Event.CTRL_MASK));
        
        // Save file
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"SaveQuery.gif"));
        imgIcon16 = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"SaveQuery16.gif"));
        saveQueryAction = new AbstractAction("Save", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    boolean bResult = saveToNewFile();

                // ????????????????
                    if(bResult) {
                        // operation successful
                    } else {
                        // something's wrong....
                    }
                
                }
            };
        
        button = toolBar.add(saveQueryAction);
        if(bToolBarWithText) {
            button.setText("Save");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Save Query");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(saveQueryAction);
        menuItem.setIcon(imgIcon16);
        menuItem.setMnemonic((int)'S');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));

        //
        addSeparator();

        // Print
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"print.gif"));
        printAction = new AbstractAction("Print", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                }
            };
        button = toolBar.add(printAction);
        if(bToolBarWithText) {
            button.setText("Print");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Print");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(printAction);
        menuItem.setIcon(null);
        menuItem.setMnemonic((int)'P');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, Event.CTRL_MASK));
		
        //
        addSeparator();

        /*
        // Exit
        imgIcon = new ImageIcon(WsFrame.class.getResource("images"+fileSep+"exit.gif"));
        exitAction = new AbstractAction("Exit", imgIcon) {
                public void actionPerformed(ActionEvent e) {
                    try {
                        ((WsFrame) topFrame).closeFrame();
                        // ???????????????
                        //System.exit(0);            // close the application
                    } catch(Exception exc) {
                        exc.printStackTrace();
                    }
                }
            };
        button = toolBar.add(exitAction);
        if(bToolBarWithText) {
            button.setText("Exit");
        } else {
            button.setText(""); //an icon-only button
        }
        button.setToolTipText("Exit");
        button.setMargin(TOOLBARBUTTON_INSETS);
        menuItem = add(exitAction);
        menuItem.setIcon(null);
        //menuItem.setHorizontalTextPosition(SwingConstants.RIGHT);
        menuItem.setMnemonic((int)'X');
        */
        
        
        // Add space between toolbar groups
        toolBar.addSeparator();
    }


    //
    public boolean saveToNewFile() {
        
        fileChooser.setDialogTitle("Save As...");
        int state = fileChooser.showSaveDialog(topFrame);
        File file = fileChooser.getSelectedFile();
        
        boolean bResult = false;
        if(file != null && state == JFileChooser.APPROVE_OPTION) {
            SqlFile sqlFile = new SqlFile(file.getPath());
            // TODO: Append ".sql" if the file doesn't have an extension!
            
            //
            bResult = saveToFile(sqlFile);
            if(bResult) {
                //topFrame.writeLog("Saved to " + sqlFile.toString());
            } else {
                // something's wrong....
            }
            
        } else {
            // operation canceled.
        }
        
        return bResult;
    }


    public boolean saveToFile(SqlFile file) {
        boolean bSuccessful = false;
                    
        // [1] Open the file
        
        if(file == null) {
            // something's wrong. 
            System.err.println("Source path name not valid");
        }


        // [2] Save the data here.
        
        try {
            PrintWriter out =
                new PrintWriter(
                                new BufferedWriter(
                                                   new FileWriter(file)));
                    
            //topFrame.writeLog("Saved to " + file.toString());

            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + file.getPath());
            //topFrame.writeError(exc.toString());
        } catch(IOException exc) {
            exc.printStackTrace();
            //topFrame.writeError(exc.toString());
        }
        
        // temporary
        //JOptionPane.showMessageDialog(topFrame, "Sql File " + file.getPath() + " saved!");
        
               
        // Temporary.....        
        bSuccessful = true;
        return true;
    }

    private boolean readFromFile(SqlFile file) {
        boolean bSuccessful = false;
        
        // [1] "Parse" Sql file!!!!
        
        Document doc = null;
        if(doc != null) {
            
            // temporary
            bSuccessful = true;
        } else {
            bSuccessful = false;
        }
        
        return bSuccessful;
    }
    
    private boolean cleanUp()
    {
        // Clean up Query Pane...
        //
        return true;
    }
    
    
    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        String s = "Action event detected."
            + "    Event source: " + source.getText();
        //        textArea.append(s + newline);
    }
    
}




