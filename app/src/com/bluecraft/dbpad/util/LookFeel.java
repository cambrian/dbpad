/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: LookFeel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad.util;


import java.util.*;
import java.awt.*;
import javax.swing.*;


/**
LookFeel is a helper class 
to easily set and manipulate LookAndFeel of Java classes.
Names of LookAndFeel are defined in LookFeelList.
This code is adapted from Sun's Java on-line tutorials.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class LookFeel implements LookFeelList {

    //<Static_Fields>
    final static boolean DEBUG = false;
    //</Static_Fields>


    //<Private_Fields>
    //</Private_Fields>


    //<Protectd_Fields>
    //</Protectd_Fields>
    
    
    //<Public_Fields>
    //</Public_Fields>


    //<Static_Methods>
    public static boolean isMetalLnfSupported() {
        return (new javax.swing.plaf.metal.MetalLookAndFeel()).isSupportedLookAndFeel();
    }
    
    public static boolean isWindowsLnfSupported() {
        return (new com.sun.java.swing.plaf.windows.WindowsLookAndFeel()).isSupportedLookAndFeel();
    }
    
    public static boolean isMotifLnfSupported() {
        return (new com.sun.java.swing.plaf.motif.MotifLookAndFeel()).isSupportedLookAndFeel();
    }
    
    public static boolean isMacLnfSupported() {
        //return (new com.sun.java.swing.plaf.mac.MacLookAndFeel()).isSupportedLookAndFeel();
        return false;
    }
    //</Static_Methods>    


    //<Constructors>
    /**
    This is a "factory" class, and hence the constructor is private.
    */
    private LookFeel() {
    }    
    //</Constructors>

    
    //<Private_Methods>
    private static void setupLookFeelByClassName(String lnfClassName) {
        if(lnfClassName != null) {
            if (DEBUG) {
                System.err.println("About to request look and feel: " 
                           + lnfClassName);
            }
            try {
                UIManager.setLookAndFeel(lnfClassName);
            } catch (ClassNotFoundException e) {
                System.err.println("Couldn't find class for specified look and feel:"
                           + lnfClassName);
                System.err.println("Did you include the L&F library in the class path?");
                System.err.println("Using the default look and feel.");
            } catch (UnsupportedLookAndFeelException e) {
                System.err.println("Can't use the specified look and feel ("
                       + lnfClassName
                       + ") on this platform.");
                System.err.println("Using the default look and feel.");
            } catch (Exception e) { 
                System.err.println("Couldn't get specified look and feel ("
                           + lnfClassName
                       + "), for some reason.");
                System.err.println("Using the default look and feel.");
                e.printStackTrace();
            } 
        } else if(DEBUG) {
            System.err.println("Look And Feel Not Available!"); 
        }
    }
    //</Private_Methods>
    
    
    //<Protectd_Methods>
    //</Protectd_Methods>


    //<Public_Methods>
    /**
    Initializes the LookAndFeel to default (Metal).
    */
    public static void initializeLF() {
        initializeLF(null);
    }    

    /**
    Initializes the LookAndFeel.
    @param  lnf Name of the LookAndFeel to be set.
    */
    public static void initializeLF(String lnf) {
        String lookAndFeel = null;
    
        if (lnf != null) {
            if (lnf.equals(METAL_LF)) {
                lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
            } else if (lnf.equals(SYSTEM_LF)) {
                lookAndFeel = UIManager.getSystemLookAndFeelClassName();
            } else if (lnf.equals(MAC_LF)) {
                lookAndFeel = MAC_LF_CLASS;
            } else if (lnf.equals(WINDOWS_LF)) {
                lookAndFeel = WINDOWS_LF_CLASS;
            } else if (lnf.equals(MOTIF_LF)) {
                lookAndFeel = MOTIF_LF_CLASS;
            } else {  // default Metal
                lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
            }
        } else {  // default Metal
            lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
        }
    
        setupLookFeelByClassName(lookAndFeel);
    }
    
    /**
    Changes the LookAndFeel for a class derived from Component.
    @param  topComponent  Name of the component 
            whose LookAndFeel should be changed.
    @param  lnf  Name of the new LookAndFeel.
    */
    public static void changeLF(Component topComponent, String lnf) { 
        String lookAndFeel = null;
    
        if (lnf != null) {
            if (lnf.equals(METAL_LF)) {
                lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
            } else if (lnf.equals(SYSTEM_LF)) {
                lookAndFeel = UIManager.getSystemLookAndFeelClassName();
            } else if (lnf.equals(MAC_LF)) {
                lookAndFeel = MAC_LF_CLASS;
            } else if (lnf.equals(WINDOWS_LF)) {
                lookAndFeel = WINDOWS_LF_CLASS;
            } else if (lnf.equals(MOTIF_LF)) {
                lookAndFeel = MOTIF_LF_CLASS;
            } else {  // default Metal
                lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
            }

            setupLookFeelByClassName(lookAndFeel);
            SwingUtilities.updateComponentTreeUI(topComponent);
            // topComponent.repaint();
        }    
    }
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    First, it sets the name of the LookAndFeel,
    then initializes the LookAndFeel,
    and finally creates a simple JFrame with this LookAndFeel.
    No error checking is done.
    */
    public static void main(String[] args) {
        final String lnf = WINDOWS_LF;
        LookFeel.initializeLF(lnf);
        JFrame frame = new JFrame();
        frame.setTitle("Model Frame Demo");
        frame.setSize(450, 350);
        frame.pack();
        frame.setVisible(true);
    }
    //</Main_Method>
}




