/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: LookFeelList.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad.util;


/**
LookFeelList defines the name of available LookAndFeels.
This interface is implemented by LookFeel, which is a helper class 
to easily set and manipulate LookAndFeel of Java classes.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public interface LookFeelList {
    final static String SYSTEM_LF= "System";
    final static String METAL_LF= "Metal";
    final static String METAL_LF_NAME= "Metal Look and Feel";
    final static String METAL_LF_CLASS = "javax.swing.plaf.metal.MetalLookAndFeel";
    final static String MOTIF_LF = "Motif";
    final static String MOTIF_LF_NAME = "Motif Look and Feel";
    final static String MOTIF_LF_CLASS = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
    final static String WINDOWS_LF = "Windows";
    final static String WINDOWS_LF_NAME = "Windows Look and Feel";
    final static String WINDOWS_LF_CLASS = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
    final static String MAC_LF = "Mac";
    final static String MAC_LF_NAME = "Mac Look and Feel";
    final static String MAC_LF_CLASS = "com.sun.java.swing.plaf.mac.macLookAndFeel";

}    




