/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: StatusBar.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;


/**
StatusPanel is used as a status bar.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class StatusBar extends JPanel implements StaticStringList {
    
    ResourceBundle RB = ResourceBundle.getBundle("com.bluecraft.dbpad.resource.SbResBundle");
    
    private JLabel statusBarText = new JLabel();
    
    private JLabel dummyIndicator = new JLabel();
    private boolean bDummy = true;
    static private String dummyText = "Indicator";
    static private Icon   dummyIconOn = new ImageIcon(StatusBar.class.getResource("images"+fileSep+"blueball.gif"));
    static private Icon   dummyIconOff = new ImageIcon(StatusBar.class.getResource("images"+fileSep+"greenball.gif"));

    private JLabel inSyncIndicator = new JLabel();
    static private String inSyncText = "In Sync";
    static private Icon   inSyncIconOn = new ImageIcon(StatusBar.class.getResource("images"+fileSep+"redball.gif"));
    static private Icon   inSyncIconOff = new ImageIcon(StatusBar.class.getResource("images"+fileSep+"yellowball.gif"));
    static private Dimension  indicatorDimension = new Dimension(80,20);
    
    public StatusBar() {

        statusBarText.setText(RB.getString("statusBarText"));
        statusBarText.setHorizontalAlignment(JLabel.LEFT);
        
        dummyIndicator.setIcon(dummyIconOn);
        dummyIndicator.setText(dummyText);
        dummyIndicator.setToolTipText(RB.getString("dummyIndicatorToolTip"));
        dummyIndicator.setHorizontalAlignment(JLabel.CENTER);
        //dummyIndicator.setPreferredSize(indicatorDimension);
        
        inSyncIndicator.setIcon(inSyncIconOff);
        inSyncIndicator.setText(inSyncText);
        inSyncIndicator.setToolTipText(RB.getString("inSyncIndicatorToolTip"));
        inSyncIndicator.setHorizontalAlignment(JLabel.CENTER);
        //inSyncIndicator.setPreferredSize(indicatorDimension);
        
        JPanel indicatorPane = new JPanel();
        //indicatorPane.setBorder(BorderFactory.createEtchedBorder());
        indicatorPane.setLayout(new BoxLayout(indicatorPane, BoxLayout.X_AXIS));
        
        indicatorPane.add(new JSeparator(JSeparator.VERTICAL));
        indicatorPane.add(Box.createRigidArea(new Dimension(3,0)));
        indicatorPane.add(dummyIndicator);
        indicatorPane.add(Box.createRigidArea(new Dimension(3,0)));
        indicatorPane.add(new JSeparator(JSeparator.VERTICAL));
        indicatorPane.add(Box.createRigidArea(new Dimension(3,0)));
        indicatorPane.add(inSyncIndicator);
        indicatorPane.add(Box.createRigidArea(new Dimension(3,0)));
        //indicatorPane.add(new JSeparator(JSeparator.VERTICAL));
        
        setBorder(BorderFactory.createEtchedBorder());
        setLayout(new BorderLayout());
        add(Box.createRigidArea(new Dimension(0,3)), BorderLayout.NORTH);
        add(Box.createRigidArea(new Dimension(0,3)), BorderLayout.SOUTH);
        add(Box.createRigidArea(new Dimension(5,0)), BorderLayout.WEST);
        add(statusBarText, BorderLayout.CENTER);
        add(indicatorPane, BorderLayout.EAST);
        
    }
    
    ///
    public void setStatus(String strText) {
        statusBarText.setText(strText);
    }
    
    ///
    public void turnOnDummyIndicator() {
        turnOnDummyIndicator(true);
    }
    public void turnOffDummyIndicator() {
        turnOnDummyIndicator(false);
    }
    public void turnOnDummyIndicator(boolean bOn) {
        if(bOn) {
            bDummy = true;
        } else {
            bDummy = false;
        } 
        invalidateDummyIndicator();
    }
    public void toggleDummyIndicator() {
        if(bDummy) {
            bDummy = false;
        } else {
            bDummy = true;
        } 
        invalidateDummyIndicator();
    }
    private void invalidateDummyIndicator() {
        if(bDummy) {
            dummyIndicator.setIcon(dummyIconOn);
        } else {
            dummyIndicator.setIcon(dummyIconOff);
        } 
        invalidate();
    }
    
    ///
    public void invalidateInSyncIndicator() {
        MainFrame topFrame = (MainFrame) getTopLevelAncestor();
        if(topFrame.bSynchronizing) {
            inSyncIndicator.setIcon(inSyncIconOn);
        } else {
            inSyncIndicator.setIcon(inSyncIconOff);
        } 
        invalidate();
    }
}





