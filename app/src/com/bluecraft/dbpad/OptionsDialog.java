/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: OptionsDialog.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;


/**
OptionsDialog is derived from TabbedDialog, 
which is a superclass of (almost) all dialog windows of DBPad in Java.

Work in progress....

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class OptionsDialog extends TabbedDialog {

    public OptionsDialog() {
        this(null);
    }
    
    public OptionsDialog(Frame frame) {
        super(frame); 

        // TabbedDialog is non-resizable. Set OptionsDialog's size here
        setSize(new Dimension(500,400));
        
        // Add tab windows
        ///////////////////////////////////////  Temporary
        // [0] test tab
        JPanel pane0 = new JPanel();
        //pane0.setBackground(BG_COLOR);
        // Put it inside a scroll pane
		////JScrollPane scrollPane0 = new JScrollPane(pane0);
		////scrollPane.setPreferredSize(new Dimension(1000,1000));
        ImageIcon imgIcon = new ImageIcon(OptionsDialog.class.getResource("images"+fileSep+"tabicon.gif"));
        ////tabbedPane.addTab("Test", imgIcon, scrollPane0, "Test Tab");
        tabbedPane.addTab("Test0", imgIcon, pane0, "Test Tab");
        
        // [1] test tab
        JPanel pane1 = new JPanel();
        tabbedPane.addTab("Test1", imgIcon, pane1, "Test Tab");
        
        // [2] test tab
        JPanel pane2 = new JPanel();
        tabbedPane.addTab("Test2", imgIcon, pane2, "Test Tab");
        ///////////////////////////////////////  Temporary

        // [3] test tab
        JPanel pane3 = new JPanel();
        tabbedPane.addTab("Test3", imgIcon, pane3, "Test Tab");
        ///////////////////////////////////////  Temporary

        
        
        // Button action listners
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Do something...
                
                // hide the dialog
                //OptionsDialog.this.setVisible(false);
                hideDialogWindow();
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // hide the dialog (do not dismiss it)
                //OptionsDialog.this.setVisible(false);
                hideDialogWindow();
            }
        });
        applyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Do something...
                
            }
        });
        helpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Bring up help window (context-sensitive)
                
            }
        });
        
    }

    

    ////////////////////////////////////////
    public static void main(String[] args) {
        OptionsDialog dialog = new OptionsDialog();
        dialog.showDialogWindow();
    }
    ////////////////////////////////////////

}

 



