/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ColorEditorKit.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import javax.swing.text.*;




/**
ColorEditorKit is inherited from JTextPane and implements syntax-coloring.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ColorEditorKit extends StyledEditorKit {

    public ColorEditorKit() {
        super();
    }

    // temporary


}



