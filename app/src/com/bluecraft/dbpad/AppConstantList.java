/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: AppConstantList.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import java.awt.*;

/**
AppConstantList lists all application default values of DBPad in Java.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public interface AppConstantList {

    // Toolbar button layouts
    final static Insets TOOLBARBUTTON_INSETS = new Insets(2,6,2,6);
    
    // Background color of the app panel
    final static Color  APPPANEL_BG_COLOR = new Color(255, 255, 255);
    
}    




