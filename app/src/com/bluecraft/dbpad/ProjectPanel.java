/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: ProjectPanel.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.event.*;
import java.util.*;


/**
ProjectPanel occupies the bottom part of the three-way split window of AppPanel,
which is the main panel of DBPad in Java.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public class ProjectPanel extends JPanel implements StaticStringList {


    JPanel labelPane = null;
    JPanel bottomPane = null;

    ProjectTree projectTree = null;

    // Store the main frame as internal variable
    //private MainFrame topFrame = null;
    private JFrame topFrame = null;
    
    public ProjectPanel() {
        this(null);
    }
    public ProjectPanel(JFrame frame) {
        super();
        //topFrame = (MainFrame) frame;
        topFrame = frame;
        
        //setBackground(Color.white);
        setLayout(new BorderLayout());
        
        
        // Add top panels
        labelPane = new JPanel();
        labelPane.setBackground(new Color(0,0,140));
        labelPane.setLayout(new BorderLayout());
        JLabel label = new JLabel("DB Project");
        label.setForeground(Color.white);
        labelPane.add(label);
        
        
        projectTree = new ProjectTree();
        //projectTree.setShowsRootHandles(true);
        //projectTree.setRootVisible(false);
		
		
        bottomPane = new JPanel();
        bottomPane.setLayout(new BorderLayout());
        bottomPane.add(new JScrollPane(projectTree), BorderLayout.CENTER);
        
        
        add(labelPane, BorderLayout.NORTH);
        add(bottomPane, BorderLayout.CENTER);
    }

}



