/************************************************************ 
 *  Copyright 1999-2001, Hyoungsoo Yoon. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: TabbedDialog.java,v 1.2 2001/01/29 08:40:06 hyoon Exp $ 

package com.bluecraft.dbpad;


import com.bluecraft.dbpad.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;


/**
TabbedDialog is a superclass of (almost) all dialog windows of DBPad in Java.
Its contentPane consists of upper mainPane, which contains tabbed window,
and lower bottomPane, which contains default buttons.
TabbedDialog is *declared* to be an abstract class, and cannot be instantiated.
Please derive a subclass.

@author   H. Yoon
@version  $Revision: 1.2 $
*/
public abstract class TabbedDialog extends JDialog implements StaticStringList {

    //final protected Color  BG_COLOR = new Color(240, 255, 230);
    final protected Color  BG_COLOR = Color.lightGray;

    JPanel mainPane = new JPanel();
    JPanel bottomPane = new JPanel();
    
    JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    
    
    protected JButton okButton = new JButton("OK");
    protected JButton cancelButton = new JButton("Cancel");
    protected JButton applyButton = new JButton("Apply");
    protected JButton helpButton = new JButton("Help");

    protected MainFrame topFrame = null;


    public TabbedDialog() {
        this(null);
    }
    
    public TabbedDialog(Frame frame) {
        super(frame, "DBPad in Java", true);  // Always modal!!!!
        topFrame = (MainFrame) frame;
        
		// Just hide it when the dialog's close button(x) is pressed.
		setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);

        // Make it non-resizable!!!
        setSize(new Dimension(500,400));
        setResizable(false);
        
        // TODO: How to set icon for JDialog????????????????????????
        //if(topFrame != null) {
        //    ?setIconImage?(topFrame.getIconImage());
        //}
        
        
        // Content pane = main pane + bottom pane
        Container contentPane = getContentPane();
        contentPane.setBackground(BG_COLOR);
        contentPane.setLayout(new BorderLayout());
        
        
        // tab pane
        tabbedPane.setBackground(BG_COLOR);
        // Add tabs in the derived class!!!!
                
        
        // main pane
        mainPane.setBackground(BG_COLOR);
        mainPane.setLayout(new BorderLayout());
		
		mainPane.setBorder(new EmptyBorder(5,5,5,5)); 
        mainPane.add(tabbedPane, BorderLayout.CENTER);

		
		// Initial states of buttons
        okButton.setEnabled(true);
        cancelButton.setEnabled(true);
        applyButton.setEnabled(false);   // No changes have been made *initially* (Of course!)
        helpButton.setEnabled(true);
        
        
        // Button action listners
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Do something...
                
                // hide the dialog
                //TabbedDialog.this.setVisible(false);
                hideDialogWindow();
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // hide the dialog (do not dismiss it)
                //TabbedDialog.this.setVisible(false);
                hideDialogWindow();
            }
        });
        applyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Do something...
                
            }
        });
        helpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Bring up help window (context-sensitive)
                
            }
        });
        
        // bottom pane
        bottomPane.setBackground(BG_COLOR);
        bottomPane.setLayout(new BorderLayout());
        
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout());
        buttonPane.setBackground(BG_COLOR);
		okButton.setPreferredSize(new Dimension(95,28));
		cancelButton.setPreferredSize(new Dimension(95,28));
		applyButton.setPreferredSize(new Dimension(95,28));
		helpButton.setPreferredSize(new Dimension(95,28));
		buttonPane.add(okButton);
		buttonPane.add(cancelButton);
		buttonPane.add(applyButton);
		buttonPane.add(helpButton);
		bottomPane.add(buttonPane, BorderLayout.CENTER);
		
		bottomPane.setBorder(new EmptyBorder(0,5,5,5));  // No top margin
		
        contentPane.add(mainPane, BorderLayout.CENTER);
        contentPane.add(bottomPane, BorderLayout.SOUTH);
        
    }

    
    // Center the dialog window relative to the main frame window
    private void centerDialogWindow() {
        Dimension dlgSize = getSize();
        
        if(topFrame != null) {
            Point frameLocation = topFrame.getLocation();
            Dimension frameSize = topFrame.getSize();
            setLocation((frameLocation.x + frameSize.width/2) - (dlgSize.width/2),
                        (frameLocation.y + frameSize.height/2) - (dlgSize.height/2));
        } else {
            Dimension scrnSize = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((scrnSize.width/2) - (dlgSize.width/2),
                        (scrnSize.height/2) - (dlgSize.height/2));
        }
    }

    public void showDialogWindow() {
        centerDialogWindow();
        setVisible(true);
    }
    public void hideDialogWindow() {
        // hide the dialog (do not dismiss it)
        setVisible(false);
        //dispose();
    }

    ////////////////////////////////////////
    public static void main(String[] args) {
        TabbedDialog dialog = new TabbedDialog() {
            // This is trick. TabbedDialog itself is an abstract class and cannot be instantiated.
        };
        //dialog.setSize(new Dimension(450,350));
        /* //////////////////////////////////////////////////
        dialog.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);            // close the application
            }
        });
        */ //////////////////////////////////////////////////
        dialog.showDialogWindow();
    }
    ////////////////////////////////////////

}

 



